Texture3D <float3> forecInOut;
// Samplers
SamplerState sPoint : IMMUTABLE
{
	Filter = MIN_MAG_MIP_POINT;
	AddressU = Clamp;
	AddressV = Clamp;
};

// Texture Inputs
RWTexture3D<float3> RWVolume :BACKBUFFER;

float3 InvVolumeSize : INVTARGETSIZE;
float3 VolumeSize : TARGETSIZE;

float4x4 tVol;

float3 boost = 1;
float threshold = .01;

Texture2D texRGB <string uiname="RGB";>;

//Texture2D texRGBDepth <string uiname="RGBDepth";>;
//Texture2D texWorld <string uiname="World";>;

[numthreads(8, 8, 8)]
void SceneFlowComputeShader( uint3 i : SV_DispatchThreadID ) {
	
	
	float3 pos = i;
	
	// get texture coordinate for sampling the rgb texture
	float2 texCd = ((i.xy/VolumeSize.xy)*2)-1;
	texCd=texCd*0.5*float2(1,-1)+0.5;
	
	// not sure if needed----
	float2 halfPixel = (1.0f / VolumeSize.xy) * 0.5f;
	texCd += halfPixel;
	
//	float2 texUvColor = texRGBDepth.SampleLevel(sPoint,texCd,0).rg;
//	float3 color = texRGB.SampleLevel(sPoint,texUvColor,0).xyz * float3(1,-1,1);

	float2 lookup = texRGB.SampleLevel(sPoint,texCd,0).rg;
	if(length(lookup) < threshold){
		RWVolume[pos]=forecInOut[i];
		return;
	} 
//	float3 color = float3(lookup, abs(lookup.x)) * float3(1,-1,1);
	float3 color = float3(lookup, 0) * float3(1,-1,1);
	
//	float3 pos = texWorld.SampleLevel(sPoint,texCd,0).xyz;
	
//	if(pos.z < VolumeSize.z - 12 ){
	if(pos.z != VolumeSize.z/2 + 1 ){
		RWVolume[pos]=forecInOut[i];
		return;
	} 
	
	pos = mul(float4(pos,1),tVol).xyz;
//	pos /= InvVolumeSize;
	
	RWVolume[pos]= color*boost + forecInOut[i];
	
}

technique11 SceneFlow
{
	pass P0	{SetComputeShader( CompileShader( cs_5_0, SceneFlowComputeShader() ) );}
}

