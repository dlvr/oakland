float4x4 tW : WORLD;
float4x4 tVP : VIEWPROJECTION;
float4x4 tWVP : WORLDVIEWPROJECTION;
float4x4 tWV : WORLDVIEW;
float4 RenderTargetSize : TARGETSIZE;

float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };

float alpha = 1;

float width = 0.1;

Texture2D inputTexture <string uiname="Texture";>;

SamplerState linearSampler <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};


struct vsin
{
	float4 pos : POSITION;
};

struct vs2gs
{
    float4 pos : POSITION;
};

struct GEO_OUT
{
    float4 Position : SV_POSITION;
    float2 TexCoord : TEXCOORD;
};

vs2gs VS_Pass(vsin input)
{
	//Passtrough in that case, since we will process in gs
	
	//We don't need normals we will calculate them on the fly
	vs2gs output;
//	output.pos = mul(input.pos, mul(tW,tVP));
	output.pos = input.pos;
    return output;
}

/*psIn VS(vsin input)
{
	//Standard displat, so transform as we would usually do
	psIn output;
	output.pos = mul(input.pos,tWVP);
    return output;
}*/

float3 CalcOffset(float3 line01, out float3 normalScaled)
{
	float3 dir = normalize(line01);
	
	// scale to correct window aspect ratio
//    float3 ratio = float3(RenderTargetSize.y, RenderTargetSize.x, 0);
//    ratio = normalize(ratio);
//    ratio = mul(normalize(ratio), tWVP).xyz;
//	float3 ratio = float3(1,1,1);
	
	float3 unit_z = normalize(float3(0, 0, -1));

	float3 dir_offset = dir /* * ratio*/* width;
	
	float3 normal = normalize(cross(unit_z, dir) /* * ratio*/);
	
	normalScaled = normal /* * ratio*/ * width ;	
	
	return dir_offset;
}


float eps : EPSILON = 0.000001f;

[maxvertexcount(12)]
void GS_Diag(triangle vs2gs input[3], inout TriangleStream<GEO_OUT> gsout)
{
//	GEO_OUT output;
	
	float EPSILON = 0.01f;
	
	//Grab triangles positions
	float4 t1 = input[0].pos;
    float4 t2 = input[1].pos;
	float4 t3 = input[2].pos;
	
	
	//Compute lines
	float3 l1 = t2.xyz - t1.xyz;
	float3 l2 = t3.xyz - t2.xyz;
	float3 l3 = t3.xyz - t1.xyz;
	
	//Compute edge length
	float dl1 = dot(l1,l1);
	float dl2 = dot(l2,l2);
	float dl3 = dot(l3,l3);
	
	//Get max length
	float maxdistsqr = max(max(dl1,dl2),dl3);
	
	GEO_OUT v[4];
	
	if (dl1 < maxdistsqr)
	{
		
		float3 normal;
		float3 offset = CalcOffset(l1, normal);
		
		float3 p0_ex = t1.xyz - offset;
   		float3 p1_ex = t2.xyz + offset;
		
		float w0 = t1.w;
    	float w1 = t2.w;
		
		v[0].Position = mul(float4(p0_ex - normal, 1) * w1, tWVP);
	    v[0].TexCoord = float2(0,0);
	
	    v[1].Position = mul(float4(p0_ex + normal + offset * 2, 1) * w0, tWVP);
	    v[1].TexCoord = float2(0,1);
	
	    v[2].Position = mul(float4(p1_ex + normal - offset * 2, 1) * w1, tWVP);
	    v[2].TexCoord = float2(1,1);
	
	    v[3].Position = mul(float4(p1_ex - normal, 1) * w1, tWVP);
	    v[3].TexCoord = float2(1,0);
		
		gsout.Append(v[2]);
	    gsout.Append(v[1]);
	    gsout.Append(v[0]);
	
	    gsout.RestartStrip();
	
	    gsout.Append(v[3]);
	    gsout.Append(v[2]);
	    gsout.Append(v[0]);
	
	    gsout.RestartStrip();
	}
		
	if (dl2 < maxdistsqr)
	{
		float3 normal;
		float3 offset = CalcOffset(l2, normal);
		
		float3 p0_ex = t3.xyz - offset;
   		float3 p1_ex = t2.xyz + offset;
		
		float w0 = t3.w;
    	float w1 = t2.w;
		
		v[0].Position = mul(float4(p0_ex - normal + offset *2, 1) * w0, tWVP);
	    v[0].TexCoord = float2(0,0);
	
	    v[1].Position = mul(float4(p0_ex + normal , 1) * w0, tWVP);
	    v[1].TexCoord = float2(0,1);
	
	    v[2].Position = mul(float4(p1_ex + normal , 1) * w1, tWVP);
	    v[2].TexCoord = float2(1,1);
	
	    v[3].Position = mul(float4(p1_ex - normal - offset *2, 1) * w1, tWVP);
	    v[3].TexCoord = float2(1,0);
		
		gsout.Append(v[0]);
	    gsout.Append(v[1]);
	    gsout.Append(v[2]);
	
	    gsout.RestartStrip();
	
	    gsout.Append(v[0]);
	    gsout.Append(v[2]);
	    gsout.Append(v[3]);
	
	    gsout.RestartStrip();
	}
	
	
	if (dl3 < maxdistsqr)
	{
		float3 normal;
		float3 offset = CalcOffset(l3, normal);
		
		float3 p0_ex = t3.xyz - offset;
   		float3 p1_ex = t1.xyz + offset;
		
		float w0 = t3.w;
    	float w1 = t1.w;
		
		v[0].Position = mul(float4(p0_ex - normal, 1) * w0, tWVP);
	    v[0].TexCoord = float2(0,0);
	
	    v[1].Position = mul(float4(p0_ex + normal, 1) * w0, tWVP);
	    v[1].TexCoord = float2(0,1);
	
	    v[2].Position = mul(float4(p1_ex + normal, 1) * w1, tWVP);
	    v[2].TexCoord = float2(1,1);
	
	    v[3].Position = mul(float4(p1_ex - normal, 1) * w1, tWVP);
	    v[3].TexCoord = float2(1,0);
		
		gsout.Append(v[0]);
	    gsout.Append(v[1]);
	    gsout.Append(v[2]);
	
	    gsout.RestartStrip();
	
	    gsout.Append(v[0]);
	    gsout.Append(v[2]);
	    gsout.Append(v[3]);
	
	    gsout.RestartStrip();
	}
}

[maxvertexcount(12)]
void GS_Simple(triangle vs2gs input[3], inout TriangleStream<GEO_OUT> gsout)
{
//	GEO_OUT output;
	
	float EPSILON = 0.01f;
	
	//Grab triangles positions
	float4 t1 = input[0].pos;
    float4 t2 = input[1].pos;
	float4 t3 = input[2].pos;
	
	
	//Compute lines
	float3 l1 = t2.xyz - t1.xyz;
	float3 l2 = t3.xyz - t2.xyz;
	float3 l3 = t3.xyz - t1.xyz;
	
	//Compute edge length
	float dl1 = dot(l1,l1);
	float dl2 = dot(l2,l2);
	float dl3 = dot(l3,l3);
	
	//Get max length
	float maxdistsqr = max(max(dl1,dl2),dl3);
	
	GEO_OUT v[4];
	
	if (dl1 < maxdistsqr)
	{
		
		float3 normal;
		float3 offset = CalcOffset(l1, normal);
		
		float3 p0_ex = t1.xyz - offset;
   		float3 p1_ex = t2.xyz + offset;
		
		float w0 = t1.w;
    	float w1 = t2.w;
		
		v[0].Position = mul(float4(p0_ex - normal, 1) * w1, tWVP);
	    v[0].TexCoord = float2(0,0);
	
	    v[1].Position = mul(float4(p0_ex + normal, 1) * w0, tWVP);
	    v[1].TexCoord = float2(0,1);
	
	    v[2].Position = mul(float4(p1_ex + normal, 1) * w1, tWVP);
	    v[2].TexCoord = float2(1,1);
	
	    v[3].Position = mul(float4(p1_ex - normal, 1) * w1, tWVP);
	    v[3].TexCoord = float2(1,0);
		
		gsout.Append(v[2]);
	    gsout.Append(v[1]);
	    gsout.Append(v[0]);
	
	    gsout.RestartStrip();
	
	    gsout.Append(v[3]);
	    gsout.Append(v[2]);
	    gsout.Append(v[0]);
	
	    gsout.RestartStrip();
	}
		
	if (dl2 < maxdistsqr)
	{
		float3 normal;
		float3 offset = CalcOffset(l2, normal);
		
		float3 p0_ex = t3.xyz - offset;
   		float3 p1_ex = t2.xyz + offset;
		
		float w0 = t3.w;
    	float w1 = t2.w;
		
		v[0].Position = mul(float4(p0_ex - normal, 1) * w0, tWVP);
	    v[0].TexCoord = float2(0,0);
	
	    v[1].Position = mul(float4(p0_ex + normal, 1) * w0, tWVP);
	    v[1].TexCoord = float2(0,1);
	
	    v[2].Position = mul(float4(p1_ex + normal, 1) * w1, tWVP);
	    v[2].TexCoord = float2(1,1);
	
	    v[3].Position = mul(float4(p1_ex - normal, 1) * w1, tWVP);
	    v[3].TexCoord = float2(1,0);
		
		gsout.Append(v[0]);
	    gsout.Append(v[1]);
	    gsout.Append(v[2]);
	
	    gsout.RestartStrip();
	
	    gsout.Append(v[0]);
	    gsout.Append(v[2]);
	    gsout.Append(v[3]);
	
	    gsout.RestartStrip();
	}
	
	
	if (dl3 < maxdistsqr)
	{
		float3 normal;
		float3 offset = CalcOffset(l3, normal);
		
		float3 p0_ex = t3.xyz - offset;
   		float3 p1_ex = t1.xyz + offset;
		
		float w0 = t3.w;
    	float w1 = t1.w;
		
		v[0].Position = mul(float4(p0_ex - normal, 1) * w0, tWVP);
	    v[0].TexCoord = float2(0,0);
	
	    v[1].Position = mul(float4(p0_ex + normal, 1) * w0, tWVP);
	    v[1].TexCoord = float2(0,1);
	
	    v[2].Position = mul(float4(p1_ex + normal, 1) * w1, tWVP);
	    v[2].TexCoord = float2(1,1);
	
	    v[3].Position = mul(float4(p1_ex - normal, 1) * w1, tWVP);
	    v[3].TexCoord = float2(1,0);
		
		gsout.Append(v[0]);
	    gsout.Append(v[1]);
	    gsout.Append(v[2]);
	
	    gsout.RestartStrip();
	
	    gsout.Append(v[0]);
	    gsout.Append(v[2]);
	    gsout.Append(v[3]);
	
	    gsout.RestartStrip();
	}
}

float4 PS(GEO_OUT input): SV_Target
{
	float4 col = inputTexture.Sample(linearSampler,input.TexCoord.xy) * cAmb;
	col.a *= alpha;
	return col;
}

technique10 Border
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS_Pass() ) );
		SetGeometryShader( CompileShader( gs_4_0, GS_Simple() ) );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
	}
}

technique10 BorderVariant
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS_Pass() ) );
		SetGeometryShader( CompileShader( gs_4_0, GS_Diag() ) );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
	}
}
