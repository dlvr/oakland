# Oakland

This repo for Oakland - Sense of Place

---------------------------------

### Date:
- Dave, 17.01.19: The building will be ready for the LED screen install to begin on May 1, which means our content install would be sometime early-to-mid May
- Resolution: w:2469px h:2100px


---------------------------------
### Start App:
- Main Patch (Client) -> RenderFade Branch
- Data Patch (Server)-> Dataserver Branch

*Master Branach obsolete*


---------------------------------
#### Raspi:
- Remote controll Frequenzies (done)
- Start up on power supply
- VNC (done) --> https://www.realvnc.com/de/connect/download/viewer/
- FFT (done) --> https://gist.github.com/ZWMiller/53232427efc5088007cab6feee7c6e4c
- Watchdog for GQRX


### Animation aprox (3-4 min):
- Ari: 2 Realtime
- Sebastiano:1 Realtime - 1 Pre-Rendert
- Kyle: 1 realtime - 2 Pre-Rendert
- Simon: 1 Realtime
- Sebo: n Realtime

### Steam audio from Raspi to VVVV
- http://iltabiai.github.io/2018/03/17/rpi-stream.html
- https://wiki.videolan.org/Documentation:Streaming_HowTo/Command_Line_Examples/

