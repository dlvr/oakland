@echo off
timeout 120
rem **** git reset --hard ***
rem **** git pull ***

powershell.exe -ExecutionPolicy Bypass -File SERVER\router_server\assets\mail\reboot-server.ps1
rem ****Set here your vvvv path****
set patchvvvv=vvvv_50alpha38.2_x64\vvvv.exe

rem ****Set here your delays****
set delay_startscript=10
set delay_checkwatchdogfile=60
set delay_startpatch=440

:setup

set CRASHCOUNTER=0
mode con:cols=70 lines=12
color E0
echo 0 > watchdog.txt
echo Please, keep this window alive !
echo %patchvvvv%
echo =====================================================================
echo starting script in %delay_startscript% seconds
echo =====================================================================
timeout %delay_startscript%
cls
set /p VAR= < watchdog.txt
set start=%TIME%
if %VAR% equ 0 (
	goto startVvvv
) else (
	goto main
)

:startVvvv
color E0
echo Please, keep this window alive !
echo %patchvvvv%
echo =====================================================================
echo Starting Vvvv...
echo Enabling watchdog in %delay_startpatch% seconds
echo =====================================================================
start "" "%patchvvvv%" /o "%~dp0\\SERVER\router_server\server_router.v4p" /allowmultiple
timeout 2
timeout %delay_startpatch%
cls
goto main

:Vvvvpresent
color A0
echo Please, keep this window alive !
echo %patchvvvv%
echo =====================================================================
echo Vvvv started since %start%
echo PID %VAR%
echo Patch has crashed %CRASHCOUNTER% times
echo Checking watchdog file in %delay_checkwatchdogfile% seconds
echo =====================================================================
echo 0 > watchdog.txt
timeout %delay_checkwatchdogfile%
cls
goto main

:Vvvvabsent
color C0
set /a CRASHCOUNTER+=1
powershell.exe -ExecutionPolicy Bypass -File SERVER\router_server\assets\mail\watchdog-crash-server.ps1
echo Please, keep this window alive !
echo %patchvvvv%
echo =====================================================================
echo Vvvv crashed, freezed or Watchdog (Windows) are not in your patch !
echo Restarting Vvvv in 5 seconds
echo =====================================================================
taskkill /F /IM vvvv.exe
timeout 5
shutdown.exe /r /t 00
cls
rem *** goto startVvvv ***

:main
set /p VAR= < watchdog.txt 
if %VAR% gtr 0 (
	goto Vvvvpresent
) else (
	goto Vvvvabsent
)

goto main