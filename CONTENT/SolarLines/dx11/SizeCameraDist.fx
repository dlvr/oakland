
#include "particle.fxh"

RWStructuredBuffer<particle> Output : BACKBUFFER;

float3 CamPos;
StructuredBuffer<float> RadiusBuffer;
float factor = 0.5;

// Constants (tweakable):
float minPointScale = 0.1;
float maxPointScale = 0.7;
float maxDistance   = 100.0;
float gamma = .45;
float radius = 0.1;
int endfade = 2;
int beginfade = 2;

[numthreads(128,1,1)]
void CSsizepow( uint3 dtid : SV_DispatchThreadID)
{ 

	float3 pos = Output[dtid.x].pos;
	float dist = pow((length(Output[dtid.x].pos - CamPos)),gamma);
	
	float pointScale =  (dist / maxDistance);
    pointScale = max(pointScale, minPointScale);
  	pointScale = min(pointScale, maxPointScale);
	Output[dtid.x].mrl.y = pow(pointScale,factor)*RadiusBuffer[dtid.x];;
	//Output[dtid.x].mrl.y = lerp(0,pointScale,factor);
	
}

[numthreads(128,1,1)]
void CSsizelerp( uint3 dtid : SV_DispatchThreadID)
{ 

	float3 pos = Output[dtid.x].pos;
	float dist = pow((length(Output[dtid.x].pos - CamPos)),gamma);
	float pointScale =  (dist / maxDistance);
	pointScale = max(pointScale, minPointScale);
  	pointScale = min(pointScale, maxPointScale);
	float size = pow(pointScale,factor);
	
	
	float thisAge = Output[dtid.x].age%Output[dtid.x].mrl.z;

	float thisRadius = radius*RadiusBuffer[dtid.x];
	

	if 	(thisAge <= (0 + beginfade)) {
		float factor1 = saturate((thisAge / beginfade));
		thisRadius = lerp(0,thisRadius,factor1);
	}
	
	 if (thisAge > (Output[dtid.x].mrl.z - endfade)) {
		float factor = saturate((Output[dtid.x].mrl.z-thisAge) / endfade);
		thisRadius = lerp(0,thisRadius,factor);
	}
	
	Output[dtid.x].mrl.y = thisRadius;
	
}


technique11 DistSize1
{
		pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, CSsizepow() ) );
	}
}

technique11 DistSize2
{
		pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, CSsizelerp() ) );
	}
}









