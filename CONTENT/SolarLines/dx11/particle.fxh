

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass,radius,life
	float age;
};
