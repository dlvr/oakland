
float4x4 tW : WORLD;
float4x4 tV : VIEW;
float4x4 tVP : VIEWPROJECTION;
float4x4 tVI : VIEWINVERSE;

Texture2D texture2d;

SamplerState linearSampler 
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};
#include "particle.fxh"


StructuredBuffer<particle> ppos;
StructuredBuffer<float4> sbColor;

float radius = 0.05f;
float alpha = 1.0;
float baseVel;
int colorcount = 1;
float4 c <bool color=true;> = 1;
float4 c0 <bool color=true;> = 1;
float4 c1 <bool color=true;> = 1;

float3 g_positions[4]:IMMUTABLE =
    {
        float3( -1, 1, 0 ),
        float3( 1, 1, 0 ),
        float3( -1, -1, 0 ),
        float3( 1, -1, 0 ),
    };

float2 g_texcoords[4]:IMMUTABLE =
    { 
        float2(0,1), 
        float2(1,1),
        float2(0,0),
        float2(1,0),
    };

SamplerState g_samLinear : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

struct VS_IN
{
	uint iv : SV_VertexID;
};

struct vs2ps
{
    float4 PosWVP: SV_POSITION;	
	float2 TexCd : TEXCOORD0;
	float2 age: TEXCOORD1;
	float2 acc: TEXCOORD2;
	float2 vel: TEXCOORD3;
	float4 Color: TEXCOORD4;
};

vs2ps VS(VS_IN input)
{
    //inititalize all fields of output struct with 0
    vs2ps Out = (vs2ps)0;
	float3 p = ppos[input.iv].pos;
	//float4 Vcol = ppos[input.iv].col;
	//float radius = ppos[input.iv].mass;
	
	float radius = (ppos[input.iv].mrl.y);  // apply to size
	float a = ppos[input.iv].mrl.z;
	//Out.Vcol = Vcol;
    Out.PosWVP = float4(p,1);
	Out.Color = sbColor[input.iv % colorcount];
	Out.age.x = ppos[input.iv].age;	
	Out.age.y = ppos[input.iv].mrl.y;
	Out.vel.x = ppos[input.iv].vel.x;
	Out.acc.x = ppos[input.iv].acc;
	Out.acc.y = ppos[input.iv].acc;


	
    return Out;
}

[maxvertexcount(4)]
void GS(point vs2ps input[1], inout TriangleStream<vs2ps> SpriteStream)
{
    vs2ps output;
    
    //
    // Emit two new triangles
    //
    for(int i=0; i<4; i++)
    {
    	
      	 float3 position = (g_positions[i]* input[0].age.y)*radius; //radius
    	// float3 position = g_positions[i]*radius;
    	
    	
        position = mul( position, (float3x3)tVI ) + input[0].PosWVP.xyz;
    	float3 norm = mul(float3(0,0,-1),(float3x3)tVI );
        output.PosWVP = mul( float4(position,1.0), mul (tW,tVP) );
        output.age = input[0].age;
    	output.acc = input[0].acc;
    	output.vel = input[0].vel;
        output.TexCd = g_texcoords[i];	
    	output.Color = input[0].Color;

        SpriteStream.Append(output);
    }
    SpriteStream.RestartStrip();
}


float4 PS_Tex(vs2ps In): SV_Target
{
    if(length(In.TexCd-.5)>.5){discard;}
	float4 col = texture2d.Sample( g_samLinear, In.TexCd)*alpha;
	col *= c;
	col.a = alpha;
	
	col *= lerp(c0,c1,(((In.vel.x-baseVel)*-200)));

	return col;
}

float4 PS_Tex1(vs2ps In): SV_Target
{
    if(length(In.TexCd-.5)>.5){discard;}
	
	
	 float4 col = texture2d.Sample( g_samLinear, In.TexCd) * In.Color;
	 col.a = alpha;
	 col *= lerp(c0,c1,(((In.vel.x-baseVel)*-200)));
    return col;
	
	
}

technique10 Draw
{
	pass P0
	{
		
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetGeometryShader( CompileShader( gs_4_0, GS() ) );
		SetPixelShader(  CompileShader(ps_4_0, PS_Tex() ) );
	}
}




technique10 Draw2
{
	pass P0
	{
		
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetGeometryShader( CompileShader( gs_4_0, GS() ) );
		SetPixelShader(  CompileShader(ps_4_0, PS_Tex1() ) );
	}
}




