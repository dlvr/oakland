
#include "particle.fxh"


StructuredBuffer<particle> PartStruct;

RWStructuredBuffer<float3> PositionOut : POSBUFFER;
//RWStructuredBuffer<uint> AgeOut : AGEBUFFER;
RWStructuredBuffer<float3> LifeOut : LIFEBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void PostionOut( uint3 DTid : SV_DispatchThreadID )
{

	PositionOut[DTid.x]= PartStruct[DTid.x].pos;
	//AgeOut[DTid.x] = PartStruct[DTid.x].age;
	LifeOut[DTid.x] = PartStruct[DTid.x].mrl;
	
}

technique10 BufferOut
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, PostionOut() ) );
	}
}




