
////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Volume Texture Distance Function
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fSampleVolDist_1_"

// ensures the function is defined only once per instance
#ifndef fSampleVolDist_1_BODY 
#define fSampleVolDist_1_BODY

#ifndef SDF_FXH
#include <packs\happy.fxh\sdf.fxh>
#endif

// Paramaters
float4x4 fSampleVolDist_1_InvMat : fSampleVolDist_1_INVMAT;
Texture3D fSampleVolDist_1_dVol : fSampleVolDist_1_DVOL;
SamplerState fSampleVolDist_1_Samp : Immutable;


float fSampleVolDist_1_ (float3 p)
{
	return fDistVolume(p, fSampleVolDist_1_dVol, fSampleVolDist_1_Samp, fSampleVolDist_1_InvMat);
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fCombineSDF3D_3_INPUT1
#define fCombineSDF3D_3_INPUT1 fSampleVolDist_1_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Noise Basis Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fNoiseS3D_7_"

// ensures the function is defined only once per instance
#ifndef fNoiseS3D_7_BODY 
#define fNoiseS3D_7_BODY

#ifndef NOISE_FXH
#include <packs\happy.fxh\noise.fxh>
#endif

#ifndef MAP_FXH
#include <packs\happy.fxh\map.fxh>
#endif

#define fNoiseS3D_7_NOISEBASIS simplex
#define fNoiseS3D_7_INFLECTION 2 // preprocessor options from patch inserted here
#ifndef fNoiseS3D_7_NOISEBASIS
#define fNoiseS3D_7_NOISEBASIS perlin
#endif

#ifndef fNoiseS3D_7_WORLEYOPTIONS
#define fNoiseS3D_7_WORLEYOPTIONS
#endif

#ifndef fNoiseS3D_7_INFLECTION
#define fNoiseS3D_7_INFLECTION 0
#endif

// paramaters
float fNoiseS3D_7_freq : fNoiseS3D_7_FREQ = 2; 
float fNoiseS3D_7_amp : fNoiseS3D_7_AMP = .1; 
float fNoiseS3D_7_center : fNoiseS3D_7_CENTER; 
float fNoiseS3D_7_bias : fNoiseS3D_7_BIAS = 0.5;
float3 fNoiseS3D_7_domainOffset : fNoiseS3D_7_DOMAINOFFSET;
	


float fNoiseS3D_7_ (float3 p)
{
	p = p * fNoiseS3D_7_freq + fNoiseS3D_7_domainOffset;
	float noise = fNoiseS3D_7_NOISEBASIS(p  fNoiseS3D_7_WORLEYOPTIONS);
	noise = sign(noise) * bias(abs(noise), fNoiseS3D_7_bias);
	#if fNoiseS3D_7_INFLECTION == 1
	// Billow
	noise = abs(noise);
	#elif fNoiseS3D_7_INFLECTION == 2
	//Ridge
	noise = 1 - abs(noise);
	#endif
	return noise * noise * fNoiseS3D_7_amp + fNoiseS3D_7_center;
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fMFBMNoiseS3D_1_INPUT
#define fMFBMNoiseS3D_1_INPUT fNoiseS3D_7_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Scalar FBM Fractal Sum
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fMFBMNoiseS3D_1_"

// ensures the function is defined only once per instance
#ifndef fMFBMNoiseS3D_1_BODY 
#define fMFBMNoiseS3D_1_BODY

#ifndef NOISE_FXH
#include <packs\happy.fxh\noise.fxh>
#endif

// Input fCombineSDF3D_3_INPUT2 function placeholder
#ifndef fMFBMNoiseS3D_1_INPUT
#define fMFBMNoiseS3D_1_INPUT length
#endif



// parameters
float fMFBMNoiseS3D_1_persistence : fMFBMNoiseS3D_1_PERSISTENCE = 0.5;  
float fMFBMNoiseS3D_1_lacunarity : fMFBMNoiseS3D_1_LACUNARITY = 2;    
int fMFBMNoiseS3D_1_octaves: fMFBMNoiseS3D_1_OCTAVES = 4;  


float fMFBMNoiseS3D_1_ (float3 p)
{
	MFBM(fMFBMNoiseS3D_1_noise, fMFBMNoiseS3D_1_INPUT, p, fMFBMNoiseS3D_1_persistence, fMFBMNoiseS3D_1_lacunarity, fMFBMNoiseS3D_1_octaves);     
	return fMFBMNoiseS3D_1_noise;
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fCombineSDF3D_3_INPUT2
#define fCombineSDF3D_3_INPUT2 fMFBMNoiseS3D_1_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D SDF Combination Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fCombineSDF3D_3_"

// ensures the function is defined only once per instance
#ifndef fCombineSDF3D_3_BODY 
#define fCombineSDF3D_3_BODY

#ifndef SDF_FXH
#include <packs\happy.fxh\sdf.fxh>
#endif

#define fCombineSDF3D_3_OP fOpEngrave(fCombineSDF3D_3_INPUT1(p), fCombineSDF3D_3_INPUT2(p), fCombineSDF3D_3_p1) // preprocessor options from patch inserted here
#ifndef fCombineSDF3D_3_OP 
#define fCombineSDF3D_3_OP U(fCombineSDF3D_3_INPUT1(p), fCombineSDF3D_3_INPUT2(p))
#endif



// Input function placeholder
#ifndef fCombineSDF3D_3_INPUT1
#define fCombineSDF3D_3_INPUT1 length
#endif

// Input class placeholder
#ifndef fCombineSDF3D_3_INPUT2
#define fCombineSDF3D_3_INPUT2 length
#endif

// Paramaters
float fCombineSDF3D_3_p1 : fCombineSDF3D_3_P1;
float fCombineSDF3D_3_p2 : fCombineSDF3D_3_P2;

float fCombineSDF3D_3_ (float3 p)
{
		return fCombineSDF3D_3_OP; // #defined in patch
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fInputMorphS3D_2_INPUT1
#define fInputMorphS3D_2_INPUT1 fCombineSDF3D_3_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Volume Texture Distance Function
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fSampleVolDist_1_"

// ensures the function is defined only once per instance
#ifndef fSampleVolDist_1_BODY 
#define fSampleVolDist_1_BODY

#ifndef SDF_FXH
#include <packs\happy.fxh\sdf.fxh>
#endif

// Paramaters
float4x4 fSampleVolDist_1_InvMat : fSampleVolDist_1_INVMAT;
Texture3D fSampleVolDist_1_dVol : fSampleVolDist_1_DVOL;
SamplerState fSampleVolDist_1_Samp : Immutable;


float fSampleVolDist_1_ (float3 p)
{
	return fDistVolume(p, fSampleVolDist_1_dVol, fSampleVolDist_1_Samp, fSampleVolDist_1_InvMat);
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fCombineSDF3D_4_INPUT1
#define fCombineSDF3D_4_INPUT1 fSampleVolDist_1_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Noise Basis Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fNoiseS3D_4_"

// ensures the function is defined only once per instance
#ifndef fNoiseS3D_4_BODY 
#define fNoiseS3D_4_BODY

#ifndef NOISE_FXH
#include <packs\happy.fxh\noise.fxh>
#endif

#ifndef MAP_FXH
#include <packs\happy.fxh\map.fxh>
#endif

#define fNoiseS3D_4_NOISEBASIS simplex
#define fNoiseS3D_4_INFLECTION 1 // preprocessor options from patch inserted here
#ifndef fNoiseS3D_4_NOISEBASIS
#define fNoiseS3D_4_NOISEBASIS perlin
#endif

#ifndef fNoiseS3D_4_WORLEYOPTIONS
#define fNoiseS3D_4_WORLEYOPTIONS
#endif

#ifndef fNoiseS3D_4_INFLECTION
#define fNoiseS3D_4_INFLECTION 0
#endif

// paramaters
float fNoiseS3D_4_freq : fNoiseS3D_4_FREQ = 2; 
float fNoiseS3D_4_amp : fNoiseS3D_4_AMP = .1; 
float fNoiseS3D_4_center : fNoiseS3D_4_CENTER; 
float fNoiseS3D_4_bias : fNoiseS3D_4_BIAS = 0.5;
float3 fNoiseS3D_4_domainOffset : fNoiseS3D_4_DOMAINOFFSET;
	


float fNoiseS3D_4_ (float3 p)
{
	p = p * fNoiseS3D_4_freq + fNoiseS3D_4_domainOffset;
	float noise = fNoiseS3D_4_NOISEBASIS(p  fNoiseS3D_4_WORLEYOPTIONS);
	noise = sign(noise) * bias(abs(noise), fNoiseS3D_4_bias);
	#if fNoiseS3D_4_INFLECTION == 1
	// Billow
	noise = abs(noise);
	#elif fNoiseS3D_4_INFLECTION == 2
	//Ridge
	noise = 1 - abs(noise);
	#endif
	return noise * noise * fNoiseS3D_4_amp + fNoiseS3D_4_center;
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fScalarOpS3D_0_INPUT1
#define fScalarOpS3D_0_INPUT1 fNoiseS3D_4_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Noise Basis Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fNoiseS3D_7_"

// ensures the function is defined only once per instance
#ifndef fNoiseS3D_7_BODY 
#define fNoiseS3D_7_BODY

#ifndef NOISE_FXH
#include <packs\happy.fxh\noise.fxh>
#endif

#ifndef MAP_FXH
#include <packs\happy.fxh\map.fxh>
#endif

#define fNoiseS3D_7_NOISEBASIS simplex
#define fNoiseS3D_7_INFLECTION 2 // preprocessor options from patch inserted here
#ifndef fNoiseS3D_7_NOISEBASIS
#define fNoiseS3D_7_NOISEBASIS perlin
#endif

#ifndef fNoiseS3D_7_WORLEYOPTIONS
#define fNoiseS3D_7_WORLEYOPTIONS
#endif

#ifndef fNoiseS3D_7_INFLECTION
#define fNoiseS3D_7_INFLECTION 0
#endif

// paramaters
float fNoiseS3D_7_freq : fNoiseS3D_7_FREQ = 2; 
float fNoiseS3D_7_amp : fNoiseS3D_7_AMP = .1; 
float fNoiseS3D_7_center : fNoiseS3D_7_CENTER; 
float fNoiseS3D_7_bias : fNoiseS3D_7_BIAS = 0.5;
float3 fNoiseS3D_7_domainOffset : fNoiseS3D_7_DOMAINOFFSET;
	


float fNoiseS3D_7_ (float3 p)
{
	p = p * fNoiseS3D_7_freq + fNoiseS3D_7_domainOffset;
	float noise = fNoiseS3D_7_NOISEBASIS(p  fNoiseS3D_7_WORLEYOPTIONS);
	noise = sign(noise) * bias(abs(noise), fNoiseS3D_7_bias);
	#if fNoiseS3D_7_INFLECTION == 1
	// Billow
	noise = abs(noise);
	#elif fNoiseS3D_7_INFLECTION == 2
	//Ridge
	noise = 1 - abs(noise);
	#endif
	return noise * noise * fNoiseS3D_7_amp + fNoiseS3D_7_center;
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fScalarOpS3D_0_INPUT2
#define fScalarOpS3D_0_INPUT2 fNoiseS3D_7_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Scalar Field Basic Ops Functions + - * /
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fScalarOpS3D_0_"

// ensures the function is defined only once per instance
#ifndef fScalarOpS3D_0_BODY 
#define fScalarOpS3D_0_BODY

#define fScalarOpS3D_0_OP fScalarOpS3D_0_INPUT1(p) * fScalarOpS3D_0_INPUT2(p) // preprocessor options from patch inserted here
#ifndef fScalarOpS3D_0_OP
#define fScalarOpS3D_0_OP fScalarOpS3D_0_INPUT1(p) + fScalarOpS3D_0_INPUT2(p); 
#endif

// Parameters
float fScalarOpS3D_0_Default : fScalarOpS3D_0_DEFAULT;
float fScalarOpS3D_0_reverseOrder : fScalarOpS3D_0_REVERSEORDER;

// Input function placeholder
#ifndef fScalarOpS3D_0_INPUT1
#define fScalarOpS3D_0_INPUT1 length
#endif

// Input 2 Defualt option
#ifndef fScalarOpS3D_0_INPUT2
float fScalarOpS3D_0_singleValue (float3 p)
{
	return fScalarOpS3D_0_Default; // if a second input class is not provided defualt to semantic
}
#define fScalarOpS3D_0_INPUT2 fScalarOpS3D_0_singleValue
#endif

float fScalarOpS3D_0_ (float3 p)
{
	return fScalarOpS3D_0_OP; 
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fCombineSDF3D_4_INPUT2
#define fCombineSDF3D_4_INPUT2 fScalarOpS3D_0_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D SDF Combination Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fCombineSDF3D_4_"

// ensures the function is defined only once per instance
#ifndef fCombineSDF3D_4_BODY 
#define fCombineSDF3D_4_BODY

#ifndef SDF_FXH
#include <packs\happy.fxh\sdf.fxh>
#endif

#define fCombineSDF3D_4_OP fOpEngrave(fCombineSDF3D_4_INPUT1(p), fCombineSDF3D_4_INPUT2(p), fCombineSDF3D_4_p1) // preprocessor options from patch inserted here
#ifndef fCombineSDF3D_4_OP 
#define fCombineSDF3D_4_OP U(fCombineSDF3D_4_INPUT1(p), fCombineSDF3D_4_INPUT2(p))
#endif



// Input function placeholder
#ifndef fCombineSDF3D_4_INPUT1
#define fCombineSDF3D_4_INPUT1 length
#endif

// Input class placeholder
#ifndef fCombineSDF3D_4_INPUT2
#define fCombineSDF3D_4_INPUT2 length
#endif

// Paramaters
float fCombineSDF3D_4_p1 : fCombineSDF3D_4_P1;
float fCombineSDF3D_4_p2 : fCombineSDF3D_4_P2;

float fCombineSDF3D_4_ (float3 p)
{
		return fCombineSDF3D_4_OP; // #defined in patch
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fInputMorphS3D_2_INPUT2
#define fInputMorphS3D_2_INPUT2 fCombineSDF3D_4_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////
//
//		Morph between two 3D Scalar Field functions by a third function (or defualt value)
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fInputMorphS3D_2_"

// ensures the function is defined only once per instance
#ifndef fInputMorphS3D_2_BODY 
#define fInputMorphS3D_2_BODY

// Parameters
float fInputMorphS3D_2_Default : fInputMorphS3D_2_DEFAULT = 0.5;

// Input function placeholder
#ifndef fInputMorphS3D_2_INPUT1
#define fInputMorphS3D_2_INPUT1 length
#endif

// Input function placeholder
#ifndef fInputMorphS3D_2_INPUT2
#define fInputMorphS3D_2_INPUT2 length
#endif

// morph defualt
#ifndef fInputMorphS3D_2_BLEND
float fInputMorphS3D_2_DefualtBlend (float3 p)
{
	return fInputMorphS3D_2_Default; // if a blend field class is not provided defualt to value semantic
}
#define fInputMorphS3D_2_BLEND fInputMorphS3D_2_DefualtBlend
#endif


float fInputMorphS3D_2_ (float3 p)
{
	if(saturate(fInputMorphS3D_2_BLEND(p)) == 0) return fInputMorphS3D_2_INPUT1(p);
	else if(saturate(fInputMorphS3D_2_BLEND(p)) == 1) return fInputMorphS3D_2_INPUT2(p);
	else return lerp(fInputMorphS3D_2_INPUT1(p), fInputMorphS3D_2_INPUT2(p), saturate(fInputMorphS3D_2_BLEND(p))); //
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fInputMorphS3D_4_INPUT1
#define fInputMorphS3D_4_INPUT1 fInputMorphS3D_2_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Volume Texture Distance Function
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fSampleVolDist_1_"

// ensures the function is defined only once per instance
#ifndef fSampleVolDist_1_BODY 
#define fSampleVolDist_1_BODY

#ifndef SDF_FXH
#include <packs\happy.fxh\sdf.fxh>
#endif

// Paramaters
float4x4 fSampleVolDist_1_InvMat : fSampleVolDist_1_INVMAT;
Texture3D fSampleVolDist_1_dVol : fSampleVolDist_1_DVOL;
SamplerState fSampleVolDist_1_Samp : Immutable;


float fSampleVolDist_1_ (float3 p)
{
	return fDistVolume(p, fSampleVolDist_1_dVol, fSampleVolDist_1_Samp, fSampleVolDist_1_InvMat);
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fCombineSDF3D_5_INPUT1
#define fCombineSDF3D_5_INPUT1 fSampleVolDist_1_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Noise Basis Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fNoiseS3D_8_"

// ensures the function is defined only once per instance
#ifndef fNoiseS3D_8_BODY 
#define fNoiseS3D_8_BODY

#ifndef NOISE_FXH
#include <packs\happy.fxh\noise.fxh>
#endif

#ifndef MAP_FXH
#include <packs\happy.fxh\map.fxh>
#endif

#define fNoiseS3D_8_NOISEBASIS simplex
#define fNoiseS3D_8_INFLECTION 2 // preprocessor options from patch inserted here
#ifndef fNoiseS3D_8_NOISEBASIS
#define fNoiseS3D_8_NOISEBASIS perlin
#endif

#ifndef fNoiseS3D_8_WORLEYOPTIONS
#define fNoiseS3D_8_WORLEYOPTIONS
#endif

#ifndef fNoiseS3D_8_INFLECTION
#define fNoiseS3D_8_INFLECTION 0
#endif

// paramaters
float fNoiseS3D_8_freq : fNoiseS3D_8_FREQ = 2; 
float fNoiseS3D_8_amp : fNoiseS3D_8_AMP = .1; 
float fNoiseS3D_8_center : fNoiseS3D_8_CENTER; 
float fNoiseS3D_8_bias : fNoiseS3D_8_BIAS = 0.5;
float3 fNoiseS3D_8_domainOffset : fNoiseS3D_8_DOMAINOFFSET;
	


float fNoiseS3D_8_ (float3 p)
{
	p = p * fNoiseS3D_8_freq + fNoiseS3D_8_domainOffset;
	float noise = fNoiseS3D_8_NOISEBASIS(p  fNoiseS3D_8_WORLEYOPTIONS);
	noise = sign(noise) * bias(abs(noise), fNoiseS3D_8_bias);
	#if fNoiseS3D_8_INFLECTION == 1
	// Billow
	noise = abs(noise);
	#elif fNoiseS3D_8_INFLECTION == 2
	//Ridge
	noise = 1 - abs(noise);
	#endif
	return noise * noise * fNoiseS3D_8_amp + fNoiseS3D_8_center;
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fCombineSDF3D_5_INPUT2
#define fCombineSDF3D_5_INPUT2 fNoiseS3D_8_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D SDF Combination Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fCombineSDF3D_5_"

// ensures the function is defined only once per instance
#ifndef fCombineSDF3D_5_BODY 
#define fCombineSDF3D_5_BODY

#ifndef SDF_FXH
#include <packs\happy.fxh\sdf.fxh>
#endif

#define fCombineSDF3D_5_OP fOpGroove(fCombineSDF3D_5_INPUT1(p), fCombineSDF3D_5_INPUT2(p), fCombineSDF3D_5_p1, fCombineSDF3D_5_p2) // preprocessor options from patch inserted here
#ifndef fCombineSDF3D_5_OP 
#define fCombineSDF3D_5_OP U(fCombineSDF3D_5_INPUT1(p), fCombineSDF3D_5_INPUT2(p))
#endif



// Input function placeholder
#ifndef fCombineSDF3D_5_INPUT1
#define fCombineSDF3D_5_INPUT1 length
#endif

// Input class placeholder
#ifndef fCombineSDF3D_5_INPUT2
#define fCombineSDF3D_5_INPUT2 length
#endif

// Paramaters
float fCombineSDF3D_5_p1 : fCombineSDF3D_5_P1;
float fCombineSDF3D_5_p2 : fCombineSDF3D_5_P2;

float fCombineSDF3D_5_ (float3 p)
{
		return fCombineSDF3D_5_OP; // #defined in patch
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fInputMorphS3D_4_INPUT2
#define fInputMorphS3D_4_INPUT2 fCombineSDF3D_5_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////
//
//		Morph between two 3D Scalar Field functions by a third function (or defualt value)
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fInputMorphS3D_4_"

// ensures the function is defined only once per instance
#ifndef fInputMorphS3D_4_BODY 
#define fInputMorphS3D_4_BODY

// Parameters
float fInputMorphS3D_4_Default : fInputMorphS3D_4_DEFAULT = 0.5;

// Input function placeholder
#ifndef fInputMorphS3D_4_INPUT1
#define fInputMorphS3D_4_INPUT1 length
#endif

// Input function placeholder
#ifndef fInputMorphS3D_4_INPUT2
#define fInputMorphS3D_4_INPUT2 length
#endif

// morph defualt
#ifndef fInputMorphS3D_4_BLEND
float fInputMorphS3D_4_DefualtBlend (float3 p)
{
	return fInputMorphS3D_4_Default; // if a blend field class is not provided defualt to value semantic
}
#define fInputMorphS3D_4_BLEND fInputMorphS3D_4_DefualtBlend
#endif


float fInputMorphS3D_4_ (float3 p)
{
	if(saturate(fInputMorphS3D_4_BLEND(p)) == 0) return fInputMorphS3D_4_INPUT1(p);
	else if(saturate(fInputMorphS3D_4_BLEND(p)) == 1) return fInputMorphS3D_4_INPUT2(p);
	else return lerp(fInputMorphS3D_4_INPUT1(p), fInputMorphS3D_4_INPUT2(p), saturate(fInputMorphS3D_4_BLEND(p))); //
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef SF3D
#define SF3D fInputMorphS3D_4_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Noise Basis Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fNoiseS3D_0_"

// ensures the function is defined only once per instance
#ifndef fNoiseS3D_0_BODY 
#define fNoiseS3D_0_BODY

#ifndef NOISE_FXH
#include <packs\happy.fxh\noise.fxh>
#endif

#ifndef MAP_FXH
#include <packs\happy.fxh\map.fxh>
#endif

#define fNoiseS3D_0_NOISEBASIS worleyFast
#define fNoiseS3D_0_INFLECTION 2 // preprocessor options from patch inserted here
#ifndef fNoiseS3D_0_NOISEBASIS
#define fNoiseS3D_0_NOISEBASIS perlin
#endif

#ifndef fNoiseS3D_0_WORLEYOPTIONS
#define fNoiseS3D_0_WORLEYOPTIONS
#endif

#ifndef fNoiseS3D_0_INFLECTION
#define fNoiseS3D_0_INFLECTION 0
#endif

// paramaters
float fNoiseS3D_0_freq : fNoiseS3D_0_FREQ = 2; 
float fNoiseS3D_0_amp : fNoiseS3D_0_AMP = .1; 
float fNoiseS3D_0_center : fNoiseS3D_0_CENTER; 
float fNoiseS3D_0_bias : fNoiseS3D_0_BIAS = 0.5;
float3 fNoiseS3D_0_domainOffset : fNoiseS3D_0_DOMAINOFFSET;
	


float fNoiseS3D_0_ (float3 p)
{
	p = p * fNoiseS3D_0_freq + fNoiseS3D_0_domainOffset;
	float noise = fNoiseS3D_0_NOISEBASIS(p  fNoiseS3D_0_WORLEYOPTIONS);
	noise = sign(noise) * bias(abs(noise), fNoiseS3D_0_bias);
	#if fNoiseS3D_0_INFLECTION == 1
	// Billow
	noise = abs(noise);
	#elif fNoiseS3D_0_INFLECTION == 2
	//Ridge
	noise = 1 - abs(noise);
	#endif
	return noise * noise * fNoiseS3D_0_amp + fNoiseS3D_0_center;
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef INPUTBUMP
#define INPUTBUMP fNoiseS3D_0_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////






#define AO

#ifndef SF3D
#define SF3D length // Just a place holder
#endif

#define RAYMARCHER

#ifndef RAYMARCH_FXH
#include<packs\happy.fxh\raymarch.fxh>
#endif

#ifndef WRITEDEPTH
#define WRITEDEPTH 1
#endif


float3 lightPos;


SamplerState linearSampler : immutable <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};



cbuffer cbControls:register(b0)
{
	float4x4 tVP:VIEWPROJECTION;
	float4x4 tV:VIEW;
};


uint materialID;

//Texture2D texMask;


struct VS_IN{float4 PosO:POSITION;float4 TexCd:TEXCOORD0;};
struct VS_OUT{float4 PosWVP:SV_POSITION;float4 TexCd:TEXCOORD0;};
VS_OUT VS(VS_IN In){VS_OUT Out=(VS_OUT)0;Out.TexCd=In.TexCd;Out.PosWVP=float4(In.PosO.xy,0,1);return Out;}

struct PS_OUT
{
	float4 pos : SV_Target0;
	float4 norm : SV_Target1;
	float2 uv : SV_Target2;
	#if WRITEDEPTH == 1
	float Depth:SV_DEPTH;
	#endif
};







PS_OUT PS_GBuffer(VS_OUT In)
{
//	if(texMask.Sample(linearSampler,In.TexCd.xy).r <= 0) discard;
//	float4 tex = texMask.Sample(linearSampler,In.TexCd.xy);
	// Raymarcher 
	////////////////////////////////////////////////////////////////
	float2 uv = In.TexCd.xy; // Takes uv as input
	float3 rd, p, n;   	float z; // Outputs surface posistion(p) & normals(n), ray direction(rd) & length(z) 
	rayMarcher(uv, p, n, rd, z);
	////////////////////////////////////////////////////////////////

	PS_OUT Out;
	Out.pos = float4(p.xyz, 1);
	Out.norm = float4(n.xyz*-1, (float) materialID * 0.001);
	Out.uv = uv;

	float ao = calcAO(p,-Out.norm.xyz);

	Out.pos.a = ao;
	
	#if WRITEDEPTH == 1
	float4 PosWVP=mul(float4(p.xyz,1),tVP);
	Out.Depth=PosWVP.z/PosWVP.w;
	#endif
	
	return Out;
}



#ifndef INPUTBUMP
float flat(float3 p)
{
	return 0.001;
}
#define INPUTBUMP flat
#endif
float bumpAmt = 1.0;

PS_OUT PS_GBufferBumped(VS_OUT In)

{
//	if(texMask.Sample(linearSampler,In.TexCd.xy).r <= 0) discard;
	
	// Raymarcher 
	////////////////////////////////////////////////////////////////
	float2 uv = In.TexCd.xy; // Takes uv as input
	float3 rd, p, n;   	float z; // Outputs surface posistion(p) & normals(n), ray direction(rd) & length(z) 
	rayMarcher(uv, p, n, rd, z);
	////////////////////////////////////////////////////////////////

	PS_OUT Out;
	float bump = INPUTBUMP(p);
	float3 pp = p + bump * n * bumpAmt;
	n += calcGradS3(INPUTBUMP, pp, 0.01)* bumpAmt;
	n = normalize(n);
	Out.pos = float4(p.xyz, 1);
	Out.norm = float4(n.xyz*-1, (float) materialID * 0.001);
	Out.uv = uv;
	
	float ao = calcAO(p,-Out.norm.xyz);

	Out.pos.a = ao;
	
	#if WRITEDEPTH == 1
	float4 PosWVP=mul(float4(p.xyz,1),tVP);
	Out.Depth=PosWVP.z/PosWVP.w;
	#endif
	
	return Out;
}


struct PS_OUT_VSM
{
	float2 Color:SV_TARGET;
	#if WRITEDEPTH == 1
	float Depth:SV_DEPTH;
	#endif
};

PS_OUT_VSM PS_VSM(VS_OUT In)
{
	// Raymarcher 
	////////////////////////////////////////////////////////////////
	float2 uv = In.TexCd.xy; // Takes uv as input
	float3 rd, p, n;   	float z; // Outputs surface posistion(p) & normals(n), ray direction(rd) & length(z) 
	rayMarcher(uv, p, n, rd, z);
	////////////////////////////////////////////////////////////////
	
//	float2 c;
//	c.rgb = INPUTRGB(p);
//	c.a = alpha;
	
	
	PS_OUT_VSM Out;
	
	float4 PosWVP=mul(float4(p.xyz,1),tVP);
	float worldSpaceDistance = distance(lightPos, p.xyz);
	
	float2 depth;
	depth.x = (worldSpaceDistance / marchMaxDistance) + .001;
	depth.y = depth.x * depth.x;
	
	Out.Color = depth;
	
	#if WRITEDEPTH == 1
	PosWVP=mul(float4(p.xyz,1),tVP);
	Out.Depth=PosWVP.z/PosWVP.w;
	#endif
	
	return Out;
}


technique11 RayMarchGBuffer
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0,VS()));
		SetPixelShader(CompileShader(ps_4_0,PS_GBuffer()));
	}
}


technique11 RayMarchGBufferBump
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0,VS()));
		SetPixelShader(CompileShader(ps_4_0,PS_GBufferBumped()));
	}
}


technique11 RayMarchVSM
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0,VS()));
		SetPixelShader(CompileShader(ps_5_0,PS_VSM()));
	}
}
