//#define AO

#ifndef SF3D
#define SF3D length // Just a place holder
#endif

#define RAYMARCHER

#ifndef RAYMARCH_FXH
#include<packs\happy.fxh\raymarch.fxh>
#endif

#ifndef WRITEDEPTH
#define WRITEDEPTH 1
#endif


float3 lightPos;


SamplerState linearSampler : immutable <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};



cbuffer cbControls:register(b0)
{
	float4x4 tVP:VIEWPROJECTION;
	float4x4 tV:VIEW;
};


uint materialID;

//Texture2D texMask;


struct VS_IN{float4 PosO:POSITION;float4 TexCd:TEXCOORD0;};
struct VS_OUT{float4 PosWVP:SV_POSITION;float4 TexCd:TEXCOORD0;};
VS_OUT VS(VS_IN In){VS_OUT Out=(VS_OUT)0;Out.TexCd=In.TexCd;Out.PosWVP=float4(In.PosO.xy,0,1);return Out;}

struct PS_OUT
{
	float4 pos : SV_Target0;
	float4 norm : SV_Target1;
	float2 uv : SV_Target2;
	#if WRITEDEPTH == 1
	float Depth:SV_DEPTH;
	#endif
};







PS_OUT PS_GBuffer(VS_OUT In)
{
//	if(texMask.Sample(linearSampler,In.TexCd.xy).r <= 0) discard;
//	float4 tex = texMask.Sample(linearSampler,In.TexCd.xy);
	// Raymarcher 
	////////////////////////////////////////////////////////////////
	float2 uv = In.TexCd.xy; // Takes uv as input
	float3 rd, p, n;   	float z; // Outputs surface posistion(p) & normals(n), ray direction(rd) & length(z) 
	rayMarcher(uv, p, n, rd, z);
	////////////////////////////////////////////////////////////////

	PS_OUT Out;
	Out.pos = float4(p.xyz, 1);
	Out.norm = float4(n.xyz*-1, (float) materialID * 0.001);
	Out.uv = uv;

	//float ao = calcAO(p,-Out.norm.xyz);

	//Out.pos.a = ao;
	
	#if WRITEDEPTH == 1
	float4 PosWVP=mul(float4(p.xyz,1),tVP);
	Out.Depth=PosWVP.z/PosWVP.w;
	#endif
	
	return Out;
}



#ifndef INPUTBUMP
float flat(float3 p)
{
	return 0.001;
}
#define INPUTBUMP flat
#endif
float bumpAmt = 1.0;

PS_OUT PS_GBufferBumped(VS_OUT In)

{
//	if(texMask.Sample(linearSampler,In.TexCd.xy).r <= 0) discard;
	
	// Raymarcher 
	////////////////////////////////////////////////////////////////
	float2 uv = In.TexCd.xy; // Takes uv as input
	float3 rd, p, n;   	float z; // Outputs surface posistion(p) & normals(n), ray direction(rd) & length(z) 
	rayMarcher(uv, p, n, rd, z);
	////////////////////////////////////////////////////////////////

	PS_OUT Out;
	float bump = INPUTBUMP(p);
	float3 pp = p + bump * n * bumpAmt;
	n += calcGradS3(INPUTBUMP, pp, 0.01)* bumpAmt;
	///NORMALS INVERT
	//n = 1-normalize(n);  
	
	Out.pos = float4(p.xyz, 1);
	Out.norm = float4(n.xyz*-1, (float) materialID * 0.001);
	Out.uv = uv;
	
//	float ao = calcAO(p,-Out.norm.xyz);

//	Out.pos.a = ao;
	
	#if WRITEDEPTH == 1
	float4 PosWVP=mul(float4(p.xyz,1),tVP);
	Out.Depth=PosWVP.z/PosWVP.w;
	#endif
	
	return Out;
}


struct PS_OUT_VSM
{
	float2 Color:SV_TARGET;
	#if WRITEDEPTH == 1
	float Depth:SV_DEPTH;
	#endif
};

PS_OUT_VSM PS_VSM(VS_OUT In)
{
	// Raymarcher 
	////////////////////////////////////////////////////////////////
	float2 uv = In.TexCd.xy; // Takes uv as input
	float3 rd, p, n;   	float z; // Outputs surface posistion(p) & normals(n), ray direction(rd) & length(z) 
	rayMarcher(uv, p, n, rd, z);
	////////////////////////////////////////////////////////////////
	
//	float2 c;
//	c.rgb = INPUTRGB(p);
//	c.a = alpha;
	
	
	PS_OUT_VSM Out;
	
	float4 PosWVP=mul(float4(p.xyz,1),tVP);
	float worldSpaceDistance = distance(lightPos, p.xyz);
	
	float2 depth;
	depth.x = (worldSpaceDistance / marchMaxDistance) + .001;
	depth.y = depth.x * depth.x;
	
	Out.Color = depth;
	
	#if WRITEDEPTH == 1
	PosWVP=mul(float4(p.xyz,1),tVP);
	Out.Depth=PosWVP.z/PosWVP.w;
	#endif
	
	return Out;
}


technique11 RayMarchGBuffer
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0,VS()));
		SetPixelShader(CompileShader(ps_4_0,PS_GBuffer()));
	}
}


technique11 RayMarchGBufferBump
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0,VS()));
		SetPixelShader(CompileShader(ps_4_0,PS_GBufferBumped()));
	}
}


technique11 RayMarchVSM
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0,VS()));
		SetPixelShader(CompileShader(ps_5_0,PS_VSM()));
	}
}
