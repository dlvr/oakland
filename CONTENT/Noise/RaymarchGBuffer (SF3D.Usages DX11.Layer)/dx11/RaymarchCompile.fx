
////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Volume Texture Distance Function
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fSampleVolDist_1_"

// ensures the function is defined only once per instance
#ifndef fSampleVolDist_1_BODY 
#define fSampleVolDist_1_BODY

#ifndef SDF_FXH
#include <packs\happy.fxh\sdf.fxh>
#endif

// Paramaters
float4x4 fSampleVolDist_1_InvMat : fSampleVolDist_1_INVMAT;
Texture3D fSampleVolDist_1_dVol : fSampleVolDist_1_DVOL;
SamplerState fSampleVolDist_1_Samp : Immutable;


float fSampleVolDist_1_ (float3 p)
{
	return fDistVolume(p, fSampleVolDist_1_dVol, fSampleVolDist_1_Samp, fSampleVolDist_1_InvMat);
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fCombineSDF3D_1_INPUT1
#define fCombineSDF3D_1_INPUT1 fSampleVolDist_1_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Noise Basis Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fNoiseS3D_4_"

// ensures the function is defined only once per instance
#ifndef fNoiseS3D_4_BODY 
#define fNoiseS3D_4_BODY

#ifndef NOISE_FXH
#include <packs\happy.fxh\noise.fxh>
#endif

#ifndef MAP_FXH
#include <packs\happy.fxh\map.fxh>
#endif

#define fNoiseS3D_4_NOISEBASIS perlin
#define fNoiseS3D_4_INFLECTION 0 // preprocessor options from patch inserted here
#ifndef fNoiseS3D_4_NOISEBASIS
#define fNoiseS3D_4_NOISEBASIS perlin
#endif

#ifndef fNoiseS3D_4_WORLEYOPTIONS
#define fNoiseS3D_4_WORLEYOPTIONS
#endif

#ifndef fNoiseS3D_4_INFLECTION
#define fNoiseS3D_4_INFLECTION 0
#endif

// paramaters
float fNoiseS3D_4_freq : fNoiseS3D_4_FREQ = 2; 
float fNoiseS3D_4_amp : fNoiseS3D_4_AMP = .1; 
float fNoiseS3D_4_center : fNoiseS3D_4_CENTER; 
float fNoiseS3D_4_bias : fNoiseS3D_4_BIAS = 0.5;
float3 fNoiseS3D_4_domainOffset : fNoiseS3D_4_DOMAINOFFSET;
	


float fNoiseS3D_4_ (float3 p)
{
	p = p * fNoiseS3D_4_freq + fNoiseS3D_4_domainOffset;
	float noise = fNoiseS3D_4_NOISEBASIS(p  fNoiseS3D_4_WORLEYOPTIONS);
	noise = sign(noise) * bias(abs(noise), fNoiseS3D_4_bias);
	#if fNoiseS3D_4_INFLECTION == 1
	// Billow
	noise = abs(noise);
	#elif fNoiseS3D_4_INFLECTION == 2
	//Ridge
	noise = 1 - abs(noise);
	#endif
	return noise * noise * fNoiseS3D_4_amp + fNoiseS3D_4_center;
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef fCombineSDF3D_1_INPUT2
#define fCombineSDF3D_1_INPUT2 fNoiseS3D_4_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D SDF Combination Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fCombineSDF3D_1_"

// ensures the function is defined only once per instance
#ifndef fCombineSDF3D_1_BODY 
#define fCombineSDF3D_1_BODY

#ifndef SDF_FXH
#include <packs\happy.fxh\sdf.fxh>
#endif

#define fCombineSDF3D_1_OP fOpEngrave(fCombineSDF3D_1_INPUT1(p), fCombineSDF3D_1_INPUT2(p), fCombineSDF3D_1_p1) // preprocessor options from patch inserted here
#ifndef fCombineSDF3D_1_OP 
#define fCombineSDF3D_1_OP U(fCombineSDF3D_1_INPUT1(p), fCombineSDF3D_1_INPUT2(p))
#endif



// Input function placeholder
#ifndef fCombineSDF3D_1_INPUT1
#define fCombineSDF3D_1_INPUT1 length
#endif

// Input class placeholder
#ifndef fCombineSDF3D_1_INPUT2
#define fCombineSDF3D_1_INPUT2 length
#endif

// Paramaters
float fCombineSDF3D_1_p1 : fCombineSDF3D_1_P1;
float fCombineSDF3D_1_p2 : fCombineSDF3D_1_P2;

float fCombineSDF3D_1_ (float3 p)
{
		return fCombineSDF3D_1_OP; // #defined in patch
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef SF3D
#define SF3D fCombineSDF3D_1_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////
//
//		3D Noise Basis Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// This token will be replaced with function name via RegExpr: "fNoiseS3D_0_"

// ensures the function is defined only once per instance
#ifndef fNoiseS3D_0_BODY 
#define fNoiseS3D_0_BODY

#ifndef NOISE_FXH
#include <packs\happy.fxh\noise.fxh>
#endif

#ifndef MAP_FXH
#include <packs\happy.fxh\map.fxh>
#endif

#define fNoiseS3D_0_NOISEBASIS worleyFast
#define fNoiseS3D_0_INFLECTION 2 // preprocessor options from patch inserted here
#ifndef fNoiseS3D_0_NOISEBASIS
#define fNoiseS3D_0_NOISEBASIS perlin
#endif

#ifndef fNoiseS3D_0_WORLEYOPTIONS
#define fNoiseS3D_0_WORLEYOPTIONS
#endif

#ifndef fNoiseS3D_0_INFLECTION
#define fNoiseS3D_0_INFLECTION 0
#endif

// paramaters
float fNoiseS3D_0_freq : fNoiseS3D_0_FREQ = 2; 
float fNoiseS3D_0_amp : fNoiseS3D_0_AMP = .1; 
float fNoiseS3D_0_center : fNoiseS3D_0_CENTER; 
float fNoiseS3D_0_bias : fNoiseS3D_0_BIAS = 0.5;
float3 fNoiseS3D_0_domainOffset : fNoiseS3D_0_DOMAINOFFSET;
	


float fNoiseS3D_0_ (float3 p)
{
	p = p * fNoiseS3D_0_freq + fNoiseS3D_0_domainOffset;
	float noise = fNoiseS3D_0_NOISEBASIS(p  fNoiseS3D_0_WORLEYOPTIONS);
	noise = sign(noise) * bias(abs(noise), fNoiseS3D_0_bias);
	#if fNoiseS3D_0_INFLECTION == 1
	// Billow
	noise = abs(noise);
	#elif fNoiseS3D_0_INFLECTION == 2
	//Ridge
	noise = 1 - abs(noise);
	#endif
	return noise * noise * fNoiseS3D_0_amp + fNoiseS3D_0_center;
}
// end of the function body
#endif 

////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef INPUTBUMP
#define INPUTBUMP fNoiseS3D_0_
#endif
////////////////////////////////////////////////////////////////////////////////////////////////






#define AO

#ifndef SF3D
#define SF3D length // Just a place holder
#endif

#define RAYMARCHER

#ifndef RAYMARCH_FXH
#include<packs\happy.fxh\raymarch.fxh>
#endif

#ifndef WRITEDEPTH
#define WRITEDEPTH 1
#endif


SamplerState linearSampler : immutable <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};



cbuffer cbControls:register(b0)
{
	float4x4 tVP:VIEWPROJECTION;
	float4x4 tV:VIEW;
};


uint materialID;

//Texture2D texMask;


struct VS_IN{float4 PosO:POSITION;float4 TexCd:TEXCOORD0;};
struct VS_OUT{float4 PosWVP:SV_POSITION;float4 TexCd:TEXCOORD0;};
VS_OUT VS(VS_IN In){VS_OUT Out=(VS_OUT)0;Out.TexCd=In.TexCd;Out.PosWVP=float4(In.PosO.xy,0,1);return Out;}

struct PS_OUT
{
	float4 pos : SV_Target0;
	float4 norm : SV_Target1;
	float2 uv : SV_Target2;
	#if WRITEDEPTH == 1
	float Depth:SV_DEPTH;
	#endif
};




PS_OUT PS_GBuffer(VS_OUT In)
{
//	if(texMask.Sample(linearSampler,In.TexCd.xy).r <= 0) discard;
//	float4 tex = texMask.Sample(linearSampler,In.TexCd.xy);
	// Raymarcher 
	////////////////////////////////////////////////////////////////
	float2 uv = In.TexCd.xy; // Takes uv as input
	float3 rd, p, n;   	float z; // Outputs surface posistion(p) & normals(n), ray direction(rd) & length(z) 
	rayMarcher(uv, p, n, rd, z);
	////////////////////////////////////////////////////////////////

	PS_OUT Out;
	Out.pos = float4(p.xyz, 1);
	Out.norm = float4(n.xyz*-1, (float) materialID * 0.001);
	Out.uv = uv;

	float ao = calcAO(p,-Out.norm.xyz);

	Out.pos.a = ao;
	
	#if WRITEDEPTH == 1
	float4 PosWVP=mul(float4(p.xyz,1),tVP);
	Out.Depth=PosWVP.z/PosWVP.w;
	#endif
	
	return Out;
}



#ifndef INPUTBUMP
float flat(float3 p)
{
	return 0.001;
}
#define INPUTBUMP flat
#endif
float bumpAmt = 1.0;

PS_OUT PS_GBufferBumped(VS_OUT In)

{
//	if(texMask.Sample(linearSampler,In.TexCd.xy).r <= 0) discard;
	
	// Raymarcher 
	////////////////////////////////////////////////////////////////
	float2 uv = In.TexCd.xy; // Takes uv as input
	float3 rd, p, n;   	float z; // Outputs surface posistion(p) & normals(n), ray direction(rd) & length(z) 
	rayMarcher(uv, p, n, rd, z);
	////////////////////////////////////////////////////////////////

	PS_OUT Out;
	float bump = INPUTBUMP(p);
	float3 pp = p + bump * n * bumpAmt;
	n += calcGradS3(INPUTBUMP, pp, 0.01)* bumpAmt;
	n = normalize(n);
	Out.pos = float4(p.xyz, 1);
	Out.norm = float4(n.xyz*-1, (float) materialID * 0.001);
	Out.uv = uv;
	
	float ao = calcAO(p,-Out.norm.xyz);

	Out.pos.a = ao;
	
	#if WRITEDEPTH == 1
	float4 PosWVP=mul(float4(p.xyz,1),tVP);
	Out.Depth=PosWVP.z/PosWVP.w;
	#endif
	
	return Out;
}


technique11 RayMarchGBuffer
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0,VS()));
		SetPixelShader(CompileShader(ps_4_0,PS_GBuffer()));
	}
}


technique11 RayMarchGBufferBump
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0,VS()));
		SetPixelShader(CompileShader(ps_4_0,PS_GBufferBumped()));
	}
}