//@author: vux
//@help: template for standard shaders
//@tags: template
//@credits: Dx11 Litsphere Catweasel based on Misak's dx9 version
#include "ColorSpace.fxh"

float normalPower <
	string UIName = "Bumpmap Strength";
	string UIWidget = "slider";
	float UIMin = 0.0;
	float UIMax = 2.0;
	float UIStep = 0.1;
> = 1.0;

float normalPower2 <
	string UIName = "Bumpmap Strength2";
	string UIWidget = "slider";
	float UIMin = 0.0;
	float UIMax = 2.0;
	float UIStep = 0.1;
> = 1.0;


float normalPower3 <
	string UIName = "Bumpmap Strength3";
	string UIWidget = "slider";
	float UIMin = 0.0;
	float UIMax = 2.0;
	float UIStep = 0.1;
> = 1.0;


bool flipGreen
<
    string UIName = "Invert Normal Map Green Channel?";
> = false;

Texture2D PosMap <string uiname="PosMap";>;
Texture2D normalMap <string uiname="normalMap";>;
//Texture2D blendMap <string uiname="Blend";>;
//Texture2D blendMap2 <string uiname="Blend2";>;
//Texture2D screenMask;
float blendfactor=1.0;
float gamma=0.0;
float blendfactor2=1.0;
float gamma2=0.0;
float3 mask={100,100,100};
float3 MaskPos={0,0,0};
SamplerState linearSampler : Immutable
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = WRAP;
    AddressV = WRAP;
};


SamplerState normalSampler : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};
Texture2D blendTexture <string uiname="RGB Blend";>;
Texture2D litSphereMap <string uiname="LitSphere";>;
Texture2D litSphereMap2 <string uiname="LitSphere2";>;
Texture2D litSphereMap3 <string uiname="LitSphere3";>;
SamplerState litSphereSampler : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = mirror;
    AddressV = mirror;
};
 
cbuffer cbPerDraw : register( b0 )
{
	float4x4 tVP : VIEWPROJECTION;	
	float4x4 tWV : WORLDVIEW;
	float4x4 tWVP : WORLDVIEWPROJECTION;
	float4 HSCB=float4(0,1,0,0);
	float4 HSCB2=float4(0,1,0,0);
	float4 HSCB3=float4(0,1,0,0);
	float blendOffset=0;
	float blendOffset2=0;
	
};

cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;

	

};
cbuffer cbTextureData : register(b2)
{
	float4x4 tTex1 <string uiname="Texture1 Transform"; bool uvspace=true; >;
	float4x4 tTex2 <string uiname="Texture2 Transform"; bool uvspace=true; >;
	float4x4 tTex3 <string uiname="Texture3 Transform"; bool uvspace=true; >;
};

struct VS_IN
{
		uint ii : SV_InstanceID;
	float4 PosO : POSITION;
	float2 TexCd : TEXCOORD0;
	//float3 Tangent  : TANGENT; //uncomment this line, save then bang the switch, sphere vanishes
//	float3 Binormal : BINORMAL; //comment it out and save, its stays gone, bang switch it reappears
	float3 Normal   : NORMAL;

};

struct vs2ps
{
    float4 PosWVP: SV_POSITION;
	float4 PosW: TEXCOORD2;
    float2 TexCd: TEXCOORD0;
	float3 Normal   : NORMAL;

};



vs2ps VS(VS_IN input)
{
    vs2ps output = (vs2ps)0;
	
	float4 pv = (input.PosO); //Transform the model
    
	output.PosWVP = pv;

    output.TexCd = input.TexCd;

	output.Normal = input.Normal;
	
    return output;
}




float4 PS(vs2ps In): SV_Target
{  
			
 float4 pos = PosMap.Sample(linearSampler,In.TexCd.xy);

	//if(pos.a <= 0 ) discard;
	if(pos.x+MaskPos.x>mask.x ) discard;
	if(pos.y+MaskPos.y>mask.y ) discard;
	if(pos.x+MaskPos.x<-mask.x ) discard;
	if(pos.y+MaskPos.y<-mask.y ) discard; 
	if(pos.z+MaskPos.z>mask.z ) discard;
	//if(pos.z<-mask.z ) discard;
	pos=mul(pos,tW);
	
	
	float blend= 0;
	float blend2= 0;
	blend= (pos.z);
	blend2= (pos.z);
	
	//float3 blendTex=normalize(blendTexture.Sample(normalSampler,In.TexCd.xy).rgb);
	
	
//float blend = blendMap.Sample(litSphereSampler,In.TexCd.xy).r;
//float blend2 = blendMap2.Sample(litSphereSampler,In.TexCd.xy).r;

 blend=max(0,min(lerp (blend,blend*blend*blend,gamma)*blendfactor+blendOffset,1));	

	
 blend2=max(0,min(lerp (blend2,blend2*blend2*blend2,gamma2)*blendfactor2+blendOffset2,1));		
	
	
	float4 texNormal = normalMap.Sample(normalSampler,In.TexCd.xy);
	if(flipGreen)texNormal.g = 1-texNormal.g;
	texNormal = texNormal*2.0-1.0;
	
	float3 worldNorm =  mul(normalize(texNormal * normalPower * 0.5 + 0.5),tTex1).xyz;
	float3 worldNorm2 = mul(normalize(texNormal * normalPower2 * 0.5 + 0.5),tTex2).xyz;
	float3 worldNorm3 = mul(normalize(texNormal * normalPower3 * 0.5 + 0.5),tTex3).xyz;

	


 float3 light  = litSphereMap.Sample (litSphereSampler,worldNorm);
 float3 light2 = litSphereMap2.Sample(litSphereSampler,worldNorm2);
 float3 light3 = litSphereMap3.Sample(litSphereSampler,worldNorm3);
	
	
	//HSCB
	float Hue=HSCB.x;
	float Saturation=HSCB.y;
	float Contrast=HSCB.z;
	float Brightness=HSCB.w;
	
	float Hue2=HSCB2.x;
	float Saturation2=HSCB2.y;
	float Contrast2=HSCB2.z;
	float Brightness2=HSCB2.w;
	
	float Hue3=HSCB3.x;
	float Saturation3=HSCB3.y;
	float Contrast3=HSCB3.z;
	float Brightness3=HSCB3.w;
	
	float3 h=RGBtoHSV(light);
    h.y=h.y*Saturation;
	float3 k0=HSVtoRGB(float3((frac(h.x+Hue)-0),h.y,h.z));
	float3 k1=HSVtoRGB(float3((frac(h.x+Hue)-1),h.y,h.z));
	light=lerp(k0,k1,pow(smoothstep(0,1,h.x),2));
	light=(lerp(light,light+.00001,light==0));
    light=normalize(light)*sqrt(3)*pow(length(light)/sqrt(3),pow(2,Contrast))*pow(2,Brightness);
	
	float3 h2=RGBtoHSV(light2);
    h2.y=h2.y*Saturation2;
	float3 k02=HSVtoRGB(float3((frac(h2.x+Hue2)-0),h2.y,h2.z));
	float3 k12=HSVtoRGB(float3((frac(h2.x+Hue2)-1),h2.y,h2.z));
	light2=lerp(k02,k12,pow(smoothstep(0,1,h2.x),2));
	light2=(lerp(light2,light2+.00001,light2==0));
    light2=normalize(light2)*sqrt(3)*pow(length(light2)/sqrt(3),pow(2,Contrast2))*pow(2,Brightness2);
	
	float3 h3=RGBtoHSV(light3);
    h3.y=h3.y*Saturation3;
	float3 k03=HSVtoRGB(float3((frac(h3.x+Hue3)-0),h3.y,h3.z));
	float3 k13=HSVtoRGB(float3((frac(h3.x+Hue3)-1),h3.y,h3.z));
	light3=lerp(k03,k13,pow(smoothstep(0,1,h3.x),2));
	light3=(lerp(light3,light3+.00001,light3==0));
    light3=normalize(light3)*sqrt(3)*pow(length(light3)/sqrt(3),pow(2,Contrast3))*pow(2,Brightness3);
	
	
	
	light=lerp(light,light3,blend2);
	
	light=lerp(light,light2,blend);
	
	
	
	
	
	
	return float4( light, 1);
		
	
}





technique11 Constant
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_5_0, VS() ) );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
	}
}




