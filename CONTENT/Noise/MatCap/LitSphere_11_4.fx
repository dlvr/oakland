//@author: vux
//@help: template for standard shaders
//@tags: template
//@credits: Dx11 Litsphere Catweasel based on Misak's dx9 version
#include "ColorSpace.fxh"

float normalPower <
	string UIName = "Bumpmap Strength";
	string UIWidget = "slider";
	float UIMin = 0.0;
	float UIMax = 2.0;
	float UIStep = 0.1;
> = 1.0;

float normalPower2 <
	string UIName = "Bumpmap Strength2";
	string UIWidget = "slider";
	float UIMin = 0.0;
	float UIMax = 2.0;
	float UIStep = 0.1;
> = 1.0;


float normalPower3 <
	string UIName = "Bumpmap Strength3";
	string UIWidget = "slider";
	float UIMin = 0.0;
	float UIMax = 2.0;
	float UIStep = 0.1;
> = 1.0;


bool flipGreen
<
    string UIName = "Invert Normal Map Green Channel?";
> = false;
/*
struct particles
{
	float3 pos;
	float3 vel;
	float3 col;
	float size;
	
};

StructuredBuffer<particles> pData;
*/
Texture2D normalMap <string uiname="normalMap";>;
Texture2D blendMap <string uiname="Blend";>;
Texture2D blendMap2 <string uiname="Blend2";>;
Texture2D screenMask;
float blendfactor=1.0;
float gamma=0.0;
float blendfactor2=1.0;
float gamma2=0.0;

float4 col1 <bool color=true;String uiname="Color1";> = { 1.0f,1.0f,1.0f,1.0f };
float4 col2 <bool color=true;String uiname="Color2";> = { 1.0f,1.0f,1.0f,1.0f };
float4 col3 <bool color=true;String uiname="Color3";> = { 1.0f,1.0f,1.0f,1.0f };
SamplerState normalSampler : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};
Texture2D litSphereMap <string uiname="LitSphere";>;
Texture2D litSphereMap2 <string uiname="LitSphere2";>;
Texture2D litSphereMap3 <string uiname="LitSphere3";>;
SamplerState litSphereSampler : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = mirror;
    AddressV = mirror;
};
 
cbuffer cbPerDraw : register( b0 )
{
	float4x4 tVP : VIEWPROJECTION;	
	float4x4 tWV : WORLDVIEW;
	float4x4 tWVP : WORLDVIEWPROJECTION;
	float4 HSCB=float4(0,1,0,0);
	float4 HSCB2=float4(0,1,0,0);
	float4 HSCB3=float4(0,1,0,0);
	
};

cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;

	
//	float4 Binormal : BINORMAL;
};

cbuffer cbTextureData : register(b2)
{
	float4x4 tTex1 <string uiname="Texture1 Transform"; bool uvspace=true; >;
	float4x4 tTex2 <string uiname="Texture2 Transform"; bool uvspace=true; >;
	float4x4 tTex3 <string uiname="Texture3 Transform"; bool uvspace=true; >;
};
struct VS_IN
{
		uint ii : SV_InstanceID;
	float4 PosO : POSITION;
	float2 TexCd : TEXCOORD0;
	//float3 Tangent  : TANGENT; //uncomment this line, save then bang the switch, sphere vanishes
//	float3 Binormal : BINORMAL; //comment it out and save, its stays gone, bang switch it reappears
	float3 Normal   : NORMAL;

};

struct vs2ps
{
    float4 PosWVP: SV_POSITION;
	float4 PosW: TEXCOORD2;
    float2 TexCd: TEXCOORD0;
	
	float3 Normal   : NORMAL;

};

vs2ps VS(VS_IN input)
{
    vs2ps output = (vs2ps)0;
	

	float4 pv = mul(input.PosO,tW); //Transform the model
    float3 vnorm =  input.Normal;

	output.PosWVP = mul(pv, tVP);
	
    output.TexCd = input.TexCd;
	

	

	output.Normal = mul(float4(vnorm,0),tWV).xyz;
	//output.Normal = mul(mul(float4(vnorm,0),tW),tVP).xyz;
	
    return output;
}




float4 PS(vs2ps In): SV_Target
{   if(screenMask.Sample(litSphereSampler,In.TexCd.xy).r <= 0) discard;
	

		// compute derivations of the world position
	float3 p_dx = ddx(In.PosWVP.xyz);
	float3 p_dy = ddy(In.PosWVP.xyz);
	// compute derivations of the texture coordinate
	float2 tc_dx = ddx(In.TexCd.xy);
	float2 tc_dy = ddy(In.TexCd.xy);
	// compute initial tangent and bi-tangent
	float3 t = normalize( tc_dy.y * p_dx - tc_dx.y * p_dy );
	float3 b = normalize( tc_dy.x * p_dx - tc_dx.x * p_dy ); // sign inversion
	// get new tangent from a given mesh normal
	float3 n = normalize(In.Normal.xyz);
	float3 x = cross(n, t);
	t = cross(x, n);
	t = normalize(t);
	// get updated bi-tangent
	x = cross(b, n);
	b = cross(n, x);
	b = normalize(b);
	
	
 float blend = blendMap.Sample(litSphereSampler,In.TexCd.xy).r;
 blend=max(0,min(lerp (blend,blend*blend*blend,gamma)*blendfactor,1));	
float blend2 = blendMap2.Sample(litSphereSampler,In.TexCd.xy).r;
	
 blend2=max(0,min(lerp (blend2,blend2*blend2*blend2,gamma2)*blendfactor2,1));		
	
	float3 texNormal = normalMap.Sample(normalSampler,In.TexCd.xy).xyz;
	if(flipGreen)texNormal.g = 1-texNormal.g;
	texNormal.rgb = texNormal.rgb*2.0-1.0;
		// Fixes normals if no normal map texture is supplied
	if ( dot(texNormal,texNormal) > 2.0 ) {
		texNormal = float3(0,0,1);
	}
	
		// The normalizes can probably go away... but meh...
    float3 T =t ;
    float3 B = b ;
    float3 N = In.Normal.xyz;

		// Put in world space and renormalize (after scaling)
    float3 worldNorm =  mul(normalize(N + texNormal.y * B* normalPower + texNormal.x * T* normalPower),tTex1);
	float3 worldNorm2 = mul(normalize(N + texNormal.y * B* normalPower2 + texNormal.x * T* normalPower2),tTex2);
	float3 worldNorm3 = mul(normalize(N + texNormal.y * B* normalPower3 + texNormal.x * T* normalPower3),tTex3);
	// Swap it around a bit... 
	//worldNorm.y = lerp(worldNorm.y,- worldNorm.y,blend);
	
//   float3 light = litSphereMap.Sample(litSphereSampler,In.TexCd.xy).xyz;

 float3 light  = litSphereMap.Sample (litSphereSampler,worldNorm.xy * 0.5 + 0.5).xyz*col1.rgb;
 float3 light2 = litSphereMap2.Sample(litSphereSampler,worldNorm2.xy * 0.5 + 0.5).xyz*col2.rgb;
 float3 light3 = litSphereMap3.Sample(litSphereSampler,worldNorm3.xy * 0.5 + 0.5).xyz*col3.rgb;
	
	
	//HSCB
	float Hue=HSCB.x;
	float Saturation=HSCB.y;
	float Contrast=HSCB.z;
	float Brightness=HSCB.w;
	
	float Hue2=HSCB2.x;
	float Saturation2=HSCB2.y;
	float Contrast2=HSCB2.z;
	float Brightness2=HSCB2.w;
	
	float Hue3=HSCB3.x;
	float Saturation3=HSCB3.y;
	float Contrast3=HSCB3.z;
	float Brightness3=HSCB3.w;
	
	float3 h=RGBtoHSV(light);
    h.y=h.y*Saturation;
	float3 k0=HSVtoRGB(float3((frac(h.x+Hue)-0),h.y,h.z));
	float3 k1=HSVtoRGB(float3((frac(h.x+Hue)-1),h.y,h.z));
	light=lerp(k0,k1,pow(smoothstep(0,1,h.x),2));
	light=(lerp(light,light+.00001,light==0));
    light=normalize(light)*sqrt(3)*pow(length(light)/sqrt(3),pow(2,Contrast))*pow(2,Brightness);
	
	float3 h2=RGBtoHSV(light2);
    h2.y=h2.y*Saturation2;
	float3 k02=HSVtoRGB(float3((frac(h2.x+Hue2)-0),h2.y,h2.z));
	float3 k12=HSVtoRGB(float3((frac(h2.x+Hue2)-1),h2.y,h2.z));
	light2=lerp(k02,k12,pow(smoothstep(0,1,h2.x),2));
	light2=(lerp(light2,light2+.00001,light2==0));
    light2=normalize(light2)*sqrt(3)*pow(length(light2)/sqrt(3),pow(2,Contrast2))*pow(2,Brightness2);
	
	float3 h3=RGBtoHSV(light3);
    h3.y=h3.y*Saturation3;
	float3 k03=HSVtoRGB(float3((frac(h3.x+Hue3)-0),h3.y,h3.z));
	float3 k13=HSVtoRGB(float3((frac(h3.x+Hue3)-1),h3.y,h3.z));
	light3=lerp(k03,k13,pow(smoothstep(0,1,h3.x),2));
	light3=(lerp(light3,light3+.00001,light3==0));
    light3=normalize(light3)*sqrt(3)*pow(length(light3)/sqrt(3),pow(2,Contrast3))*pow(2,Brightness3);
	
	
	
	light=lerp(light,light3,blend2);
	
	light=lerp(light,light2,blend);
		

	
	
	
	return float4( light, 1.0 );
}





technique11 Constant
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
	}
}




