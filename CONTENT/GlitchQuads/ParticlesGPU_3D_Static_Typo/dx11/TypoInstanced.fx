//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

Texture2D texture2d; 

SamplerState g_samLinear : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

StructuredBuffer< float4x4> sbWorld;
StructuredBuffer<float4> sbColor;
StructuredBuffer<float> sbTypo;
cbuffer cbPerDraw : register( b0 )
{
	float4x4 tVP : LAYERVIEWPROJECTION;
	float4x4 tW : WORLD;
	float4x4 tVI:VIEWINVERSE;
	
	int colorcount = 1;
};


struct VS_IN
{
	uint ii : SV_InstanceID;
	float4 PosO : POSITION;
	float2 TexCd : TEXCOORD0;

};

struct vs2ps
{
    float4 PosWVP: SV_POSITION;	
	float4 Color: TEXCOORD0;
    float2 TexCd: TEXCOORD1;
	
};
//float3x3 lookat(float3 dir,float3 up=float3(0,1,0)){float3 z=normalize(dir);float3 x=normalize(cross(up,z));float3 y=normalize(cross(z,x));return float3x3(x,y,z);} 

vs2ps VS(VS_IN input)
{
    //inititalize all fields of output struct with 0
    vs2ps Out = (vs2ps)0;
	//float3 vUp=normalize(float3(1,1,0));
	//float3 View=normalize((input.PosO.xyz-tVI[input.ii].xyz));
	//float3x3 lkt=lookat(View,vUp);
	float4x4 w = sbWorld[input.ii];
	//w*=lkt;
	float t = sbTypo[input.ii];
	w=mul(w,tW);
   // float4 Pos  = mul(float4(mul(input.PosO.xyz,lkt),1),mul(w,tVP));
	//float4 Pos  = mul(input.PosO,mul(w,tVP));
	float4 Pos  = mul(input.PosO,mul(w,tVP));
	Out.PosWVP= Pos;
	Out.Color = sbColor[input.ii % colorcount];
	float2 TCD = input.TexCd.xy /= 16.;
    TCD += float2((t%16)/16.,floor(t/16.)/16.);
    Out.TexCd = TCD;
    return Out;
}




float4 PS_Tex(vs2ps In): SV_Target
{
    float4 col = texture2d.Sample( g_samLinear, In.TexCd) * In.Color;
    return col;
}





technique10 Constant
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetPixelShader( CompileShader( ps_4_0, PS_Tex() ) );
	}
}




