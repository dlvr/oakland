
#include "particle.fxh"

//float3 influence;
float limiter = 2;
float3 SpherePosition;
float3 Motion;

RWStructuredBuffer<particle> Output : BACKBUFFER;


//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstrain( uint3 DTid : SV_DispatchThreadID )
{

	if (length(Output[DTid.x].pos-SpherePosition) >= limiter)
	{

		Output[DTid.x].pos = (normalize(Output[DTid.x].pos-SpherePosition+Motion)*limiter)+SpherePosition;
		
	}

}

technique10 Constrain
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstrain() ) );
	}
}




