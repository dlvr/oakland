
int endfade = 2;
int beginfade = 2;

#include "particle.fxh"
float radius;

StructuredBuffer<float> RadiusBuffer;

RWStructuredBuffer<particle> Output : BACKBUFFER;


//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void AgeRadius( uint3 DTid : SV_DispatchThreadID )
{
	
	float thisAge = Output[DTid.x].age%Output[DTid.x].mrl.z;

	float thisRadius = radius;
	

	if 	(thisAge <= (0 + beginfade)) {
		float factor1 = saturate((thisAge / beginfade));
		thisRadius = lerp(0,thisRadius,factor1);
	}
	
	 if (thisAge > (Output[DTid.x].mrl.z - endfade)) {
		float factor = saturate((Output[DTid.x].mrl.z-thisAge) / endfade);
		thisRadius = lerp(0,thisRadius,factor);
	}
	

	Output[DTid.x].mrl.y = thisRadius;

}

[numthreads(128, 1, 1)]
void AgeBufferRadius( uint3 DTid : SV_DispatchThreadID )
{
	
	float thisAge = Output[DTid.x].age%Output[DTid.x].mrl.z;

	float thisRadius = RadiusBuffer[DTid.x];
	

	if 	(thisAge <= (0 + beginfade)) {
		float factor1 = saturate((thisAge / beginfade));
		thisRadius = lerp(0,thisRadius,factor1);
	}
	
	 if (thisAge > (Output[DTid.x].mrl.z - endfade)) {
		float factor = saturate((Output[DTid.x].mrl.z-thisAge) / endfade);
		thisRadius = lerp(0,thisRadius,factor);
	}
	

	Output[DTid.x].mrl.y = thisRadius;

}

[numthreads(128, 1, 1)]
void FixedRadius( uint3 DTid : SV_DispatchThreadID )
{


	Output[DTid.x].mrl.y = radius;

}



[numthreads(128, 1, 1)]
void FixedBufferRadius( uint3 DTid : SV_DispatchThreadID )
{


	Output[DTid.x].mrl.y = RadiusBuffer[DTid.x];

}

technique10 AgeBased
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, AgeRadius() ) );
	}
}

technique10 AgeBufferBased
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, AgeBufferRadius() ) );
	}
}

technique10 Fixed
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, FixedRadius() ) );
	}
}

technique10 FixedBufferBased
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, FixedBufferRadius() ) );
	}
}

