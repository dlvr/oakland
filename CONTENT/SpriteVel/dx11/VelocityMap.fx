//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 
Texture2D tex <string uiname="Texture";>;

SamplerState g_samLinear : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = MIRROR;
    AddressV = MIRROR;
};

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
};

RWStructuredBuffer<particle> Output : BACKBUFFER;
//Buffer containing uvs for sampling
 StructuredBuffer<float2> uv <string uiname="UV Buffer";>;
float2 ScaleXY=1;
float fieldPower =1;
//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
		float3 p = Output[DTid.x].pos;
	
 		float2 tempP = p.xz/ScaleXY;
		
		//float4 c = tex.SampleLevel(g_samLinear,((tempP+1)/2),0);
		float4 c = tex.SampleLevel(g_samLinear,((tempP+1)/2),0);
		p.xz =(c.xy)*fieldPower;
		p.y = 0;
		//if (Output[DTid.x].pos.y > -.3)
			Output[DTid.x].acc += p* (.005*Output[DTid.x].mrl.x);
}



technique10 Constant
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




