
RWStructuredBuffer<float3> output : BACKBUFFER;

StructuredBuffer<float3> bufferIN;

uint threadCount;
float ClipPlan = 0.0;

[numthreads(128,1,1)]
void CS_BBox( uint3 dtid : SV_DispatchThreadID)
{ 
	if (dtid.x >= threadCount) { return; }
	
	output[dtid.x] = bufferIN[dtid.x];
	
	output[dtid.x].z = max(output[dtid.x].z, ClipPlan);	
	
}

technique11 BoundingBox
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CS_BBox() ) );
	}
}






