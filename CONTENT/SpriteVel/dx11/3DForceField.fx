//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
};

RWStructuredBuffer<particle> Output : BACKBUFFER;
//Buffer containing uvs for sampling
StructuredBuffer<float2> ff <string uiname="ForceField";>;
float fieldPower =1;
//==============================================================================
//COMPUTE SHADRER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	
		float3 p = Output[DTid.x].pos;
	
		float2 force = ff[DTid.x];
	
		p.xz += force*.000001 * fieldPower;
	
		Output[DTid.x].pos = p;
		//p +=(force)*.001;
	//	Output[DTid.x].pos = p;
	/*
 		float2 tempP = p.xy/ScaleXY;
		
		float4 c = tex.SampleLevel(g_samLinear,((tempP+1)/2),0);
		p +=(c.xyy-0.5)*fieldPower;
	
		if (Output[DTid.x].pos.y > -.3)
			Output[DTid.x].pos = p;
	
	*/
}



technique10 Constant
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




