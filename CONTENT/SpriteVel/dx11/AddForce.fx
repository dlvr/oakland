//@author: Noir

#include "particle.fxh"

float3 force;

RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	float3 acc = Output[DTid.x].acc;
	float3 vel = Output[DTid.x].vel;
	float mass = Output[DTid.x].mrl.x;

	acc = (mass/1000)*force;

	Output[DTid.x].acc += acc;
	
}



technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




