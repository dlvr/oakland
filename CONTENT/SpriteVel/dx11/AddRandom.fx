#include "particle.fxh"

float3 influence;
bool On = false;


RWStructuredBuffer<particle> Output : BACKBUFFER;


uint wang_hash(uint seed)
{
    seed = (seed ^ 61) ^ (seed >> 16);
    seed *= 9;
    seed = seed ^ (seed >> 4);
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> 15);
    return seed;
}

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	float3 acc = float3(0,0,0);
	float3 vel= float3(0,0,0);
	float3 force = float3(0,0,0);
	
	if (On)
	{
		force.x = float(wang_hash(DTid.x)) * (1.0 / 4294967296.0);
		force.y = float(wang_hash(DTid.x+1)) * (1.0 / 4294967296.0);
		force.z = float(wang_hash(DTid.x+2)) * (1.0 / 4294967296.0);
		acc += (lerp(-1,1,force))*influence;
	}
	else
		acc = float3(0,0,0);
	
	Output[DTid.x].acc += acc;
	
}





technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




