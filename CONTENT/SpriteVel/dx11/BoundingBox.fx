//define particle properties

//#include "particle_struct.fxh"
struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass,radius,life
	float age;
};


float3 Size;
// buffer which holds particle objects
RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSBoundingBox( uint3 index : SV_DispatchThreadID )
{
	
	if (Output[index.x].pos.x > Size.x/2)
	{
		Output[index.x].pos.x = Size.x/2;
		Output[index.x].vel.x *= -1; 
	}
	else if (Output[index.x].pos.x < -Size.x/2)
	  {
	  	Output[index.x].vel.x *= -1;
	  	Output[index.x].pos.x = -Size.x/2;
	  	
	  }
		if (Output[index.x].pos.y > Size.y/2)
	{
		Output[index.x].pos.y = Size.y/2;
		Output[index.x].vel.y *= -1; 
	}
	else if (Output[index.x].pos.y < -Size.y/2)
	  {
	  	Output[index.x].vel.y *= -1;
	  	Output[index.x].pos.y = -Size.y/2;
	  	
	  }
		if (Output[index.x].pos.z > Size.z/2)
	{
		Output[index.x].pos.z = Size.z/2;
		Output[index.x].vel.z *= -1; 
	}
	else if (Output[index.x].pos.z < -Size.z/2)
	  {
	  	Output[index.x].vel.z *= -1;
	  	Output[index.x].pos.z = -Size.z/2;
	  	
	  }
	  
	  	  
}

technique10 Bbox
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSBoundingBox() ) );
	}
}




