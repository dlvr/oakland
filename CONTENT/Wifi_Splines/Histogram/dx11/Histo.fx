//@author: everyoneishappy
//@help:  histogram calculation
//@tags: 
//@credits: mholub

RWStructuredBuffer<uint> RWValueBuffer : BACKBUFFER;


#ifndef BUCKETCOUNT 
#define BUCKETCOUNT 256
#endif

int2 Size;
int NumGroups;
Texture2D<float4> texture2d <string uiname="Texture";>;

groupshared uint data[BUCKETCOUNT];

[numthreads(BUCKETCOUNT,1,1)]
void Clear_CS(uint3 dtid : SV_DispatchThreadID)
{
	uint idx = dtid.x;
	if (idx < BUCKETCOUNT) {
		RWValueBuffer[idx] = 0;
	}
}


[numthreads(BUCKETCOUNT,1,1)]
void CS_Histogram(uint3 dtid : SV_DispatchThreadID, uint3 gtid : SV_GroupThreadID)
{
	data[gtid.x] = 0;
	AllMemoryBarrierWithGroupSync();
	uint idx = dtid.x;
	int stride = BUCKETCOUNT * NumGroups;
	while (idx < (uint)(Size.x * Size.y)) {
		uint3 uv = uint3(idx % Size.x, idx / Size.x, 0);		
		InterlockedAdd(data[texture2d.Load(uv).r * (BUCKETCOUNT - 1)], 1);
		idx += stride;
	}
	
	AllMemoryBarrierWithGroupSync();			
	InterlockedAdd(RWValueBuffer[gtid.x], data[gtid.x]);
}

technique11 Clear
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, Clear_CS() ) );
	}
}


technique11 Histogram
{
	pass P1
	{
		SetComputeShader( CompileShader( cs_5_0, CS_Histogram() ) );
	}
}