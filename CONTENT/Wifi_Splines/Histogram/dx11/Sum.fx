//@author: vux
//@help: template for simple compute shader (buffer write)
//@tags: template
//@credits: 

RWStructuredBuffer<uint> OutputBuffer : BACKBUFFER;
StructuredBuffer<uint> bucketBuffer;
uint bucketCount;


[numthreads(64,1,1)]
void CS(uint3 tid : SV_DispatchThreadID)
{
	if (tid.x >= bucketCount)
		return;
	InterlockedAdd(OutputBuffer[0], bucketBuffer[tid.x]);
}

technique11 Apply
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CS() ) );
	}
}




