//@author: vux
//@help: template for simple compute shader (buffer write)
//@tags: template
//@credits: 

RWStructuredBuffer<float> OutputBuffer : BACKBUFFER;
StructuredBuffer<uint> bucketBuffer;
uint bucketCount;
float denominator = 256.0;


[numthreads(64,1,1)]
void CS(uint3 tid : SV_DispatchThreadID)
{
	if (tid.x >= bucketCount)
		return;
	
	float bucket = float(bucketBuffer[tid.x]) / denominator;
	
	OutputBuffer[tid.x] = bucket;
}

technique11 Apply
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CS() ) );
	}
}




