Texture2D tex0 <string uiname="Texture";>;

SamplerState s0 <bool visible=false;string uiname="Sampler";>
{Filter=MIN_MAG_MIP_LINEAR;AddressU=CLAMP;AddressV=CLAMP;};

StructuredBuffer<float3> sbPos;

cbuffer cbControls:register(b0){
	float4x4 tVP:VIEWPROJECTION;
	float4x4 tWV:WORLDVIEW;
	float4x4 tWVP:WORLDVIEWPROJECTION;
	float4x4 tVPI:VIEWPROJECTIONINVERSE;
	float4x4 tWI:WORLDINVERSE;
	float4x4 tVI:VIEWINVERSE;
	float4x4 tPI:PROJECTIONINVERSE;
	float4x4 tW:WORLD;
	//float Alpha <float uimin=0.0; float uimax=1.0;> = 1; 
	//float4 Color <bool color=true;> = {1.0,1.0,1.0,1.0};
	float4 Color1 <bool color=true;> =float4(0,0,0,1);
	float4 Color2 <bool color=true;> =1;
	int Count=1;
	float Amount=1;
	float Radius <float uimin=0.0;> = 1; 
	float Gamma=1;
	//float4x4 tTex <string uiname="Texture Transform";>;
	//float4x4 tColor <string uiname="Color Transform";>;
	
};

struct VS_IN{
	float4 PosO:POSITION;
	float4 TexCd:TEXCOORD0;
};

struct VS_OUT{
	float4 PosWVP:SV_POSITION;
	float4 TexCd:TEXCOORD0;
	float4 PosW:TEXCOORD1;
};

VS_OUT VS(VS_IN In){
	VS_OUT Out=(VS_OUT)0;
	float4 PosW=mul(In.PosO,tW);
	Out.PosWVP=PosW;
	Out.PosW=PosW;
	//Out.PosWVP=mul(PosW,tVP);
	//Out.TexCd=mul(In.TexCd,tTex);
	Out.TexCd=In.TexCd;
	return Out;
}


float4 pSPOT(VS_OUT In):SV_Target{
	float4 c=1;
	c.rgb=mul(In.PosW,tVPI).xyz;
	float3 cd=normalize(mul(float4(mul(float4(In.PosW.xy,1,1),tPI).xy,1,0),tVI).xyz);
	float g=0;
	for(int i=0;i<Count;i++){
		float d=distance(normalize(sbPos[i].xyz),cd);
		g+=Amount*pow(smoothstep(2*Radius,0,d),Gamma);
	}
	float pillow=pow(pow(smoothstep(.5,0,abs(In.TexCd.y-0.5)),0.5)*pow(smoothstep(.5,0,abs(In.TexCd.x-0.5)),0.5),.1);
	//g*=pillow;
	c=lerp(Color1,Color2,g);
	//c.rgb*=pillow;
	c.a=1;
	return c;
}
float4 pSPOTGLOW(VS_OUT In):SV_Target{
	float4 c=1;
	c.rgb=mul(In.PosW,tVPI).xyz;
	float3 cd=normalize(mul(float4(mul(float4(In.PosW.xy,1,1),tPI).xy,1,0),tVI).xyz);
	float g=0;
	for(int i=0;i<Count;i++){
		float d=distance((sbPos[i].xyz),cd);
		//g+=Amount*pow(smoothstep(2*Radius,0,d),Gamma)/pow(abs(.15+d/Radius),Gamma)/(1+g);
		float spot=Amount*pow(smoothstep(2*Radius,0,d),Gamma);
		spot+=0.1*Amount*pow(smoothstep(2*Radius,0,d),Gamma)/pow(abs(.05+d/Radius),Gamma)*pow(.5,2*g);
		g+=spot;
	}
	float pillow=pow(pow(smoothstep(.5,0,abs(In.TexCd.y-0.5)),0.5)*pow(smoothstep(.5,0,abs(In.TexCd.x-0.5)),0.5),.1);
	//g*=pillow;
	c=lerp(Color1,Color2,g);
	//c.rgb*=pillow;
	c.a=1;
	return c;
}
float4 pSPOTPILLOW(VS_OUT In):SV_Target{
	float4 c=1;
	c.rgb=mul(In.PosW,tVPI).xyz;
	float3 cd=normalize(mul(float4(mul(float4(In.PosW.xy,.5,1),tPI).xy,.5,0),tVI).xyz);
	float g=0;
	for(int i=0;i<Count;i++){
		g+=Amount*pow(smoothstep(2*Radius,0,distance(normalize(sbPos[i].xyz),cd)),Gamma);
	}
	float pillow=pow(pow(smoothstep(.5,0,abs(In.TexCd.y-0.5)),0.5)*pow(smoothstep(.5,0,abs(In.TexCd.x-0.5)),0.5),.3);
	g*=pillow;
	
	c=lerp(Color1,Color2,g);
	c.rgb*=pow(pillow,.2);
	c.a=1;
	return c;
}

technique10 Spot{
	pass P0{
		SetVertexShader(CompileShader(vs_4_0,VS()));
		SetPixelShader(CompileShader(ps_4_0,pSPOT()));
	}
}
technique10 SpotGlow{
	pass P0{
		SetVertexShader(CompileShader(vs_4_0,VS()));
		SetPixelShader(CompileShader(ps_4_0,pSPOTGLOW()));
	}
}
technique10 SpotPillow{
	pass P0{
		SetVertexShader(CompileShader(vs_4_0,VS()));
		SetPixelShader(CompileShader(ps_4_0,pSPOTPILLOW()));
	}
}



