//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 
#include "particle.fxh"

float3 resetPosition;
RWStructuredBuffer<particle> Output : BACKBUFFER;
StructuredBuffer<float3> resetPos;
//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{

	
	if (Output[DTid.x].age%Output[DTid.x].mrl.z  > (Output[DTid.x].mrl.z-2 ))
	//if (Output[DTid.x].age%Output[DTid.x].mrl.z  > (Output[DTid.x].mrl.z-2))
	{
		Output[DTid.x].acc =float3 (0,0,0);
		Output[DTid.x].pos = resetPos[DTid.x];
		Output[DTid.x].age = 0;
		
	}

}


technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




