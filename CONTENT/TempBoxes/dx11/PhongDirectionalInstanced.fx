//@author: vux
//@help: template for standard shaders
//@tags: template
//@credits: 

Texture2D texture2d <string uiname="Texture";>;

SamplerState linearSampler : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};
 
cbuffer cbPerDraw : register( b0 )
{
	float4x4 tVP : VIEWPROJECTION;	
	float4x4 tV: VIEW;
	float4x4 tP: PROJECTION;
	float4x4 tWIT: WORLDINVERSETRANSPOSE;
};

cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	float4x4 tWV: WORLDVIEW;
	float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };
	float4x4 tTex <bool uvspace=true; string uiname="Texture Transform";>;
    float4x4 tColor <string uiname="Color Transform";>;
	int colorcount = 1;
};

#include "PhongDirectional.fxh"

StructuredBuffer< float4x4> sbWorld;
StructuredBuffer<float4> sbColor;

struct VS_IN
{
	uint ii : SV_InstanceID;
	float4 PosO : POSITION;
	float3 NormO: NORMAL;
	float4 TexCd : TEXCOORD0;

};

struct vs2ps
{
	float4 PosWVP: SV_POSITION;
    float4 TexCd : TEXCOORD0;
    float3 LightDirV: TEXCOORD1;
    float3 NormV: TEXCOORD2;
    float3 ViewDirV: TEXCOORD3;
	float4 Color: TEXCOORD4;
	
	
};

// -----------------------------------------------------------------------------
// VERTEXSHADERS
// -----------------------------------------------------------------------------
	
	vs2ps VS(VS_IN input)
{
    //inititalize all fields of output struct with 0
    vs2ps Out = (vs2ps)0;

    //inverse light direction in view space
    Out.LightDirV = normalize(-mul(lDir, tV));
    

    //position (projected)

	float4x4 w = sbWorld[input.ii];
	w=mul(w,tW);
    Out.PosWVP  = mul(input.PosO,mul(w,tVP));
	 //normal in view space
	//Out.NormV = normalize(mul(mul(input.NormO, (float3x3)tWIT),(float3x3)tV).xyz);
	Out.NormV = normalize(mul(mul(input.NormO, (float3x3)w),(float3x3)tV).xyz);

	Out.Color = sbColor[input.ii % colorcount];
    //Out.TexCd = input.TexCd;
    Out.TexCd = mul(input.TexCd, tTex);
	Out.ViewDirV = -normalize(mul(input.PosO, mul(w,tV)));
   // Out.ViewDirV = -normalize(mul(input.PosO, tWV));
    return Out;
}


// -----------------------------------------------------------------------------
// PIXELSHADERS:
// -----------------------------------------------------------------------------

float Alpha <float uimin=0.0; float uimax=1.0;> = 1;

float4 PS(vs2ps In): SV_Target
{
    float4 col = texture2d.Sample(linearSampler, In.TexCd.xy);
    col.rgb *= PhongDirectional(In.NormV, In.ViewDirV, In.LightDirV);
    col.a *= Alpha;
    

    return mul(col, tColor)*In.Color;
}





technique10 GouraudDirectional
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
	}
}




