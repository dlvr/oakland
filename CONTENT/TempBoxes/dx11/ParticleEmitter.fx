
//define particle properties
#include "particle.fxh"

bool reset;


StructuredBuffer<float3> resetPos;
StructuredBuffer<float3> initVelocity;
StructuredBuffer<float3> mrl;
StructuredBuffer<float> initialAge;

//Emitters Color
//StructuredBuffer<float4> emitCol;

RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	float3 vel = float3(0,0,0);
	
	
	if (reset)
	{
		
		Output[DTid.x].pos = resetPos[DTid.x];
		Output[DTid.x].mrl = mrl[DTid.x];
		Output[DTid.x].age = initialAge[DTid.x];
		Output[DTid.x].vel = float3(0,0,0);
		Output[DTid.x].acc = float3(0,0,0);
	//	Output[DTid.x].col = float4(0,0,0,0);;

	}
	else
	{
		Output[DTid.x].age ++;
	 	vel = Output[DTid.x].acc;
		Output[DTid.x].pos += vel;
		Output[DTid.x].vel = vel;
	//	float4 col = emitCol[DTid.x];
	//	Output[DTid.x].col = col;	
	}
}

technique10 Constant
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




