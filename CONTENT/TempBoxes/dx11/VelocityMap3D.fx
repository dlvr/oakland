
#include "particle.fxh"

Texture3D tex <string uiname="Texture";>;
float3 Offset;
SamplerState volumeSampler // : IMMUTABLE
{
	Filter  =MIN_MAG_MIP_LINEAR;
	AddressU = mirror;
	AddressV = mirror;
	AddressW = mirror;
};

RWStructuredBuffer<particle> Output : BACKBUFFER;
//Buffer containing uvs for sampling
StructuredBuffer<float2> uv <string uiname="UV Buffer";>;
float3 scale=1;
float fieldPower =1;
//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
		
		//float3 p = Output[DTid.x].pos;
		 float3 p = Output[DTid.x].pos;
 		float3 tempP = p.xyz*scale+Offset;
		//col = texVOL.Sample(g_samPoint,float3(In.TexCd.xyz));
		
		float4 c =  tex.SampleLevel(volumeSampler,((tempP+1)/2),0);
		//float4 c = tex.Sample(g_samLinear,((tempP+1)/2),0);
		p =(c.xyz)*fieldPower;
	
		Output[DTid.x].acc += p* (.005*Output[DTid.x].mrl.x);
		
}



technique10 Constant
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




